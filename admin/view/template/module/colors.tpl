<?php echo $header; ?>
<div id="content">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">

        <table id="module" class="list">
          <thead>
            <tr>
				<td class="center"><?php echo $color_name; ?></td>
				<td class="center">Статус</td>
              <td class="center"><?php echo $color_code; ?></td>
             <td></td>
            </tr>
          </thead>
          <?php $module_row = 0; ?>
          <?php foreach ($colors as $color) { ?>
          <tbody id="module-row<?php echo $module_row; ?>">
            <tr>
				<input style="width:150px;" type="hidden" name="colors_module[<?php echo $module_row; ?>][id]" value="<?php echo $color['id']; ?>" size="1" />
				<td class="center">
					<input style="width:150px;" type="text" name="colors_module[<?php echo $module_row; ?>][name]" value="<?php echo $color['name']; ?>" size="1" />
				</td>
				<td class="center">
					<input style="width:150px;" type="color" name="colors_module[<?php echo $module_row; ?>][code]" value="<?php echo $color['code']; ?>" size="3" />
				</td>
				<td class="center">
					<select name="colors_module[<?php echo $module_row; ?>][status]">
						<option value="1" <? if($color['status'] == 1){ ?>selected="selected"<? } ?>>Включено</option>
						<option value="0" <? if($color['status'] == 0){ ?>selected="selected"<?}?>>Выключено</option>
					</select>
				</td>
                <td class="center">
					<input style="width:150px;" type="hidden" class="del_value" name="colors_module[<?php echo $module_row; ?>][delete]" value="" size="1" />
					<a onclick="$('#module-row<?php echo $module_row; ?>').find('.del_value').val('delete');$('#module-row<?php echo $module_row; ?>').css('display', 'none');" class="button delete_color"><?php echo $button_remove; ?></a>
				</td>
            </tr>
          </tbody>
          <?php $module_row++; ?>
          <?php } ?>
          <tfoot>
            <tr>
              <td colspan="4"></td>
              <td class="left"><a onclick="addModule();" class="button"><?php echo $button_add_module; ?></a></td>
            </tr>
          </tfoot>
        </table>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript"><!--
var module_row = <?php echo $module_row; ?>;


function addModule() {	
	html  = '<tbody id="module-row' + module_row + '">';
	html += '  <tr>';
	
	html += ' <td class="center"><input style="width:150px;" type="text" name="colors_module[' + module_row + '][name]"  size="1" /></td>';
	
	html += ' <td class="center"><input style="width:150px;" type="color" name="colors_module[' + module_row + '][code]"  size="3" /></td>';
	
	html += '	<td class="center">	<select name="colors_module[' + module_row + '][status]">						<option value="1" selected="selected">Включено</option>	<option value="0">Выключено</option></select>	</td>';
	html += '    <td class="center"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#module tfoot').before(html);
	
	module_row++;
}
//--></script> 
<?php echo $footer; ?>
<?php 
class ControllerProductCategory extends Controller {  
	public function index() { 
		$this->language->load('product/category');
		
		$this->load->model('catalog/category');
		
		$this->load->model('catalog/product');
		
		$this->load->model('tool/image'); 
		
		if (isset($this->request->get['filter'])) {
			$filter = $this->request->get['filter'];
		} else {
			$filter = '';
		}
				
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.sort_order';
		}
		if (isset($this->request->get['color'])) {
			$color = $this->request->get['color'];
		} else {
			$color = '0';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else { 
			$page = 1;
		}	
							
		if (isset($this->request->get['limit'])) {
			$limit = $this->request->get['limit'];
		} else {
			$limit = $this->config->get('config_catalog_limit');
		}
							
		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
       		'separator' => false
   		);	
			
		if (isset($this->request->get['path'])) {
			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}	
			
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
									
			$path = '';
		
			$parts = explode('_', (string)$this->request->get['path']);
		
			$category_id = (int)array_pop($parts);
			
			foreach ($parts as $path_id) {
				if (!$path) {
					$path = 69;//(int)$path_id;
				} else {
					$path .= '_' . (int)$path_id;
				}
									
				$category_info = $this->model_catalog_category->getCategory($path_id);
				
				if ($category_info) {
	       			$this->data['breadcrumbs'][] = array(
   	    				'text'      => $category_info['name'],
						'href'      => $this->url->link('product/category', 'path=' . $path . $url),
        				'separator' => $this->language->get('text_separator')
        			);
				}
			}
		} else {
			$category_id = 0;
		}
				
		$category_info = $this->model_catalog_category->getCategory($category_id);
	
		if ($category_info) {
	  		if ($category_info['seo_title']) {
		  		$this->document->setTitle($category_info['seo_title']);
			} else {
		  		$this->document->setTitle($category_info['name']);
			}
			
			$this->document->setDescription($category_info['meta_description']);
			$this->document->setKeywords($category_info['meta_keyword']);
			$this->document->addScript('catalog/view/javascript/jquery/jquery.total-storage.min.js');
			$this->document->addScript('catalog/view/javascript/jquery/tabs.js');
			$this->document->addScript('catalog/view/javascript/jquery/colorbox/jquery.colorbox-min.js');
			$this->document->addStyle('catalog/view/javascript/jquery/colorbox/colorbox.css');
			
			if ($category_info['seo_h1']) {
				$this->data['heading_title'] = $category_info['seo_h1'];
			} else {
				$this->data['heading_title'] = $category_info['name'];
			}
			
			$this->data['text_contact'] = $this->language->get('text_contact');
			$this->data['text_city'] = $this->language->get('text_city');
			$this->data['text_1'] = $this->language->get('text_1');
			$this->data['text_2'] = $this->language->get('text_2');
			$this->data['entry_captcha'] = $this->language->get('entry_captcha');
			$this->data['text_telephone'] = $this->language->get('text_telephone');
			$this->data['entry_name'] = $this->language->get('entry_name');
			$this->data['entry_email'] = $this->language->get('entry_email');
			
			$this->data['text_refine'] = $this->language->get('text_refine');
			$this->data['text_empty'] = $this->language->get('text_empty');			
			$this->data['text_quantity'] = $this->language->get('text_quantity');
			$this->data['text_manufacturer'] = $this->language->get('text_manufacturer');
			$this->data['text_model'] = $this->language->get('text_model');
			$this->data['text_price'] = $this->language->get('text_price');
			$this->data['text_tax'] = $this->language->get('text_tax');
			$this->data['text_points'] = $this->language->get('text_points');
			$this->data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
			$this->data['text_display'] = $this->language->get('text_display');
			$this->data['text_list'] = $this->language->get('text_list');
			$this->data['text_grid'] = $this->language->get('text_grid');
			$this->data['text_sort'] = $this->language->get('text_sort');
			$this->data['text_limit'] = $this->language->get('text_limit');
					
			$this->data['button_quick_view'] = $this->language->get('button_quick_view');
			$this->data['button_cart'] = $this->language->get('button_cart');
			$this->data['button_wishlist'] = $this->language->get('button_wishlist');
			$this->data['button_compare'] = $this->language->get('button_compare');
			$this->data['button_continue'] = $this->language->get('button_continue');
			
			$this->data['view_subcategories'] = $this->config->get('config_view_subcategory');
			$this->data['quick_view_categories'] = $this->config->get('config_quick_view_categories');
			
			// Set the last category breadcrumb		
			$url = '';
			
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}	
			
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
			
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
									
			$this->data['breadcrumbs'][] = array(
				'text'      => $category_info['name'],
				'href'      => $this->url->link('product/category', 'path=' . $this->request->get['path']),
				'separator' => $this->language->get('text_separator')
			);
			
			if ($this->config->get('config_image_subcategory_width')) {
				$image_subcategory_width = $this->config->get('config_image_subcategory_width');
			} else {
				$image_subcategory_width = 220;
			}
			
			if ($this->config->get('config_image_subcategory_height')) {
				$image_subcategory_height = $this->config->get('config_image_subcategory_height');
			} else {
				$image_subcategory_height = 220;
			}
			
			if ($this->config->get('config_sub_category_description_limit')) {
				$sub_category_description_limit = $this->config->get('config_sub_category_description_limit');
			} else {
				$sub_category_description_limit = 300;
			}
								
			if ($category_info['image']) {
				$this->data['thumb'] = $this->model_tool_image->resize($category_info['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
			} else {
				$this->data['thumb'] = '';
			}
									
			$this->data['description'] = html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8');
			$this->data['compare'] = $this->url->link('product/compare');
			
			$url = '';
			
			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}	
						
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	
			
			if (isset($this->request->get['quantity'])) {
				$url .= '&quantity=' . $this->request->get['quantity'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}	
			
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
								
			$this->data['categories'] = array();
			
			
	/*======= получаем подкатегории ======*/
		if($category_id == 65 || $category_id == 66 || $category_id == 67){
			
			if(isset($this->request->get['availability'])){
				$availability = $this->request->get['availability'];
			}
			else{
				$availability = 0;
			}
			
			$data = array(
				'filter_category_id' => $category_id,
				'filter_filter'      => $filter, 
				'sort'               => $sort,
				'color'               => $color,
				'order'              => $order,
				'availability'		 => $availability,
				'start'              => ($page - 1) * $limit,
				'limit'              => $limit
			);
			
			$results = $this->model_catalog_product->getProducts($data);
			foreach($results as $result){
				$category[] = $this->model_catalog_product->getCategoriesProduct($result['product_id']);
				
			}
			
			foreach($category as $results){
				foreach($results as $result){
					$all_cat[] = $result;
				}
			}
			$all_cat = array_unique($all_cat);
			unset($results);
			//var_dump($all_cat);
			foreach($all_cat as $result){
				if($result != '68' && $result != '69' && $result != $category_id){
					$results[] = $this->model_catalog_category->getCategory($result);
				}
			}
			/*echo "<pre>"; var_dump($results);
			 exit;*/
			
			foreach ($results as $result) {
				$data = array(
					'filter_category_id'  => $result['category_id'],
					'filter_sub_category' => true
				);
				
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $image_subcategory_width, $image_subcategory_height);
				} else {
					$image = false;
				}
				
				if ($result['description']) {
					$description = utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $sub_category_description_limit) . '...';
				} else {
					$description = $this->language->get('text_no_description');
				}
				
				$product_total = $this->model_catalog_product->getTotalProducts($data);				
				
				$pos = strpos($result['name'], '(', 0);
				$rest1 = substr($result['name'], 0, $pos);
				$rest2 = substr($result['name'], $pos);
				
				$this->data['categories'][] = array(
					
					'name1'  	  => $rest1,// . ($this->config->get('config_product_count') ? ' (' . $product_total . ')' : ''),
					'name2'  	  => $rest2,
					'product_total' => $product_total,
					'description' => $description,
					'thumb' 	  => $image,
					'no_image' 	  => $this->model_tool_image->resize('no_image.jpg', $image_subcategory_width, $image_subcategory_height),
					'href'  	  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . $url)
				);
			} 
		}
		
			$this->data['width'] = $this->config->get('config_image_subcategory_width') + 2;
			
			if ($this->config->get('config_limit_days_new_product')) {
				$limit_days_new_product = $this->config->get('config_limit_days_new_product');
			} else {
				$limit_days_new_product = 31;
			}
			
			if ($this->config->get('config_limit_viewed_popular_product')) {
				$limit_viewed_popular_product = $this->config->get('config_limit_viewed_popular_product');
			} else {
				$limit_viewed_popular_product = 50;
			}
			
			$timestamp 			= time();
			$date_time_array 	= getdate($timestamp);
			$hours 				= $date_time_array['hours'];
			$minutes 			= $date_time_array['minutes'];
			$seconds 			= $date_time_array['seconds'];
			$month 				= $date_time_array['mon'];
			$day 				= $date_time_array['mday'];
			$year 				= $date_time_array['year'];

			$timestamp = mktime($hours, $minutes, $seconds, $month,$day - $limit_days_new_product, $year);
			
			$this->data['products'] = array();
			//var_dump($sort);
			if(isset($this->request->get['availability'])){
				$availability = $this->request->get['availability'];
			}
			else{
				$availability = 0;
			}
			if($category_id == 69){
				/*$data = array(
					//'filter_category_id' => $category_id,
					'filter_filter'      => $filter, 
					'sort'               => $sort,
					'color'               => $color,
					'order'              => $order,
					'availability'		 => $availability,
					'start'              => ($page - 1) * $limit,
					'limit'              => $limit
				);*/
				
				$caregory_list = $this->model_catalog_category->getCategories(69);
				foreach ($caregory_list as $result) {
					$data = array(
						'filter_category_id'  => $result['category_id'],
						'filter_sub_category' => true
					);
					
					if ($result['image']) {
						$image = $this->model_tool_image->resize($result['image'], $image_subcategory_width, $image_subcategory_height);
					} else {
						$image = false;
					}
					
					if ($result['description']) {
						$description = utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $sub_category_description_limit) . '...';
					} else {
						$description = $this->language->get('text_no_description');
					}
					
					$product_total = $this->model_catalog_product->getTotalProducts($data);				
					
					$pos = strpos($result['name'], '(', 0);
					$rest1 = substr($result['name'], 0, $pos);
					$rest2 = substr($result['name'], $pos);
					
					$this->data['categories'][] = array(
						
						'name1'  	  => $rest1,// . ($this->config->get('config_product_count') ? ' (' . $product_total . ')' : ''),
						'name2'  	  => $rest2,
						'product_total' => $product_total,
						'description' => $description,
						'thumb' 	  => $image,
						'no_image' 	  => $this->model_tool_image->resize('no_image.jpg', $image_subcategory_width, $image_subcategory_height),
						'href'  	  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . $url)
					);
				} 
			}
			else
			$data = array(
				'filter_category_id' => $category_id,
				'filter_filter'      => $filter, 
				'sort'               => $sort,
				'color'               => $color,
				'order'              => $order,
				'availability'		 => $availability,
				'start'              => ($page - 1) * $limit,
				'limit'              => $limit
			);
			
			
			$product_total = $this->model_catalog_product->getTotalProducts($data); 
			
			$results = $this->model_catalog_product->getProducts($data);
			//var_dump($results);
			$colors = array();
			foreach ($results as $result) {
				
				//var_dump($result['quantity']);
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
				} else {
					$image = false;
				}
				
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}
				
				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}	
				
				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}				
				
				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}
				
				if ($this->config->get('config_sticker_special_categories')) {
					if ((float)$result['special']) {
						if ($result['price'] > 0) {
							$sale = '<div class="stiker-special">' . '-' . round((($result['price'] - $result['special'])/$result['price'])*100, 0) . '<span>%</span>' . '</div>';
						} else {
							$sale = false;
						}	
					} else {
						$sale = false;
					}			
				} else {
					$sale = false;
				}
				
				if ($this->config->get('config_sticker_new_categories')) {
					if (($result['date_available']) > strftime('%Y-%m-%d',$timestamp)) {
						$new = '<div class="stiker-new"></div>';
					} else {
						$new = false;
					}
				} else {
					$new = false;
				}
				
				if ($this->config->get('config_sticker_popular_categories')) {
					if (($result['viewed']) > ($limit_viewed_popular_product)) {
						$popular = '<div class="stiker-popular"></div>';
					} else {
						$popular = false;
					}
				} else {
					$popular = false;
				}
				$result_image = array();
				$result_image = $this->model_catalog_product->getProductImages($result['product_id']);
				foreach($result_image as $result2){
					$color2[] = $this->model_catalog_product->getColorImages($result2['color_id']);
				}
				
				if(isset($result_image) && isset($result_image[0]))
				$images_prod = $this->model_catalog_product->getColorImages($result_image[0]['color_id']);
				
				if(isset($images_prod) && isset($images_prod[0])){ 
					$code_color = $images_prod[0]['code'];
					$name_color = $images_prod[0]['name'];
				}
				
				//
				foreach($color2 as $result3){
					
					$prov = 0;
					foreach($colors as $asdasd){
						if($asdasd == $result3[0]['id']){
							$prov = 1;
						}
					}
					if($prov == 0){
						$colors[] = $result3[0]['id'];
					}
				}
				$proverka_zapus = 0;
				if(isset($this->request->get['color'])){
					$dfgdfg = 0;
					foreach($color2 as $result3){
						if($this->request->get['color'] == $result3[0]['id']){
							$dfgdfg = 1;
							//echo $this->request->get['color']." == ".$result3[0]['id']." ; ".$dfgdfg."<br>";
						}
					}
					
					//echo $dfgdfg."<br>";
					
					if($dfgdfg == 1){
						$proverka_zapus = 1;
						if(isset($this->request->get['sort']) && isset($this->request->get['order']) && $this->request->get['sort'] == 'p.quantity'){
							if($this->request->get['order'] == "DESC"){
								if($result['quantity'] != 0){
									$proverka_zapus = 1;
								}
								else{
									$proverka_zapus = 0;
								}
							}
							elseif($this->request->get['order'] == "ASC"){
								if($result['quantity'] <= 1){
									$proverka_zapus = 1;
								}
								else{
									$proverka_zapus = 0;
								}
							}
						}
					}
					
				//var_dump($result);
			
				}
				elseif(isset($this->request->get['sort']) && isset($this->request->get['order']) && $this->request->get['sort'] == 'p.quantity'){
					if($this->request->get['order'] == "DESC"){
						if($result['quantity'] > 1){
							$proverka_zapus = 1;
						}
					}
					elseif($this->request->get['order'] == "ASC"){
						if($result['quantity'] <= 1){
							$proverka_zapus = 1;
						}
					}
				}
				else{
					$proverka_zapus = 1;
				}
				
				if($proverka_zapus == 1){
				
					$this->data['products'][] = array(
						//,
						'attribute_groups' => $this->model_catalog_product->getProductAttributes($result['product_id']),
						'product_id'  	   	=> $result['product_id'],
						'name'        	   	=> $result['name'],
						'quantity'			=> $result['quantity'],
						'code_color'       	=> $code_color,
						'name_color'       	=> $name_color,
						'sku'		 		=> $result['sku'],
						'thumb'       	   	=> $image,
						'no_image'    		=> $this->model_tool_image->resize('no_image.jpg', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height')),
						'description' 		=> utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_list_description_limit')) . '...',
						'description_list' 	=> utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_list_description_limit')) . '...',
						'description_grid' 	=> utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_grid_description_limit')) . '...',
						//'price'       		=> $price,
						'special'     		=> $special,
						'tax'         		=> $tax,
						'rating'      		=> $result['rating'],
						'sale' 	  	  		=> $sale,
						'new'     	  		=> $new,
						'popular'     		=> $popular,
						'reviews'     		=> sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
						'href'        		=> $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
					);
					
				}
				unset($color2);
				//unset($colors);
				
			}
			//$product_total = count($this->data['products']);
			//echo $this->request->get['sort'].$this->request->get['order'];
			
			$url = '';
			
			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}
				
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
			
			if (isset($this->request->get['color'])) {
				$url .= '&color=' . $this->request->get['color'];
				$this->data['quantity'] = $this->url->link('product/category', 'path=' . $this->request->get[						'path'] . $url."");
			}
			else{
				$this->data['quantity'] = $this->url->link('product/category', 'path=' . $this->request->get[						'path'] . "");
			}
			if($category_id == 69)
			$data2 = array(
				//'filter_category_id' => $category_id,
				'filter_filter'      => $filter, 
				'start'              => ($page - 1) * $limit,
				'limit'              => 100//$limit
			);
			else
			$data2 = array(
				'filter_category_id' => $category_id,
				'filter_filter'      => $filter, 
				'start'              => ($page - 1) * $limit,
				'limit'              => 100//$limit
			);
			
			$results2 = $this->model_catalog_product->getProducts($data2);
			//var_dump($results2);
			$colors22 = array();
			foreach($results2 as $result22){
				
				$result_image = array();
				$result_image = $this->model_catalog_product->getProductImages($result22['product_id']);
				foreach($result_image as $result2){
					$color2[] = $this->model_catalog_product->getColorImages($result2['color_id']);
				}
				
				//
				foreach($color2 as $result3){
					
					$prov = 0;
					foreach($colors22 as $asdasd){
						if($asdasd == $result3[0]['id']){
							$prov = 1;
						}
					}
					if($prov == 0){
						$colors22[] = $result3[0]['id'];
					}
				}
			}
			
			if (isset($this->request->get['availability'])) {
				$url .= '&availability=' . $this->request->get['availability'];
				$this->data['sorts'] = array();
				foreach($colors22 as $result){
					$perem123 = $this->model_catalog_product->getColorImages($result);
					//var_dump($perem123);
					$this->data['sorts'][] = array(
						'text'  => $perem123[0]['name'],
						'value' => $perem123[0]['id'],
						'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url.'&color='.$perem123[0]['id'].'')
					);
				}
				$this->data['special_sort'] = array(
						'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '')
					);
			}
			else{
				$this->data['sorts'] = array();
				foreach($colors22 as $result){
					$perem123 = $this->model_catalog_product->getColorImages($result);
					//var_dump($perem123);
					$this->data['sorts'][] = array(
						'text'  => $perem123[0]['name'],
						'value' => $perem123[0]['id'],
						'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&color='.$perem123[0]['id'].'')
					);
				}
				$this->data['special_sort'] = array(
						'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '')
					);
			}
						
			$this->data['reset_sorting'] = $this->url->link('product/category', 'path=' . $this->request->get[						'path']);
			
			
			
			/*
			
			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_default'),
				'value' => 'p.sort_order-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.sort_order&order=ASC' . $url)
			);
			
			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_name_asc'),
				'value' => 'pd.name-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=ASC' . $url)
			);

			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_name_desc'),
				'value' => 'pd.name-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=DESC' . $url)
			);

			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_price_asc'),
				'value' => 'p.price-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=ASC' . $url)
			); 

			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_price_desc'),
				'value' => 'p.price-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=DESC' . $url)
			); 
			
			if ($this->config->get('config_review_status')) {
				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_desc'),
					'value' => 'rating-DESC',
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=DESC' . $url)
				); 
				
				$this->data['sorts'][] = array(
					'text'  => $this->language->get('text_rating_asc'),
					'value' => 'rating-ASC',
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=ASC' . $url)
				);
			}
			
			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_model_asc'),
				'value' => 'p.model-ASC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=ASC' . $url)
			);

			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_model_desc'),
				'value' => 'p.model-DESC',
				'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=DESC' . $url)
			);*/
			
			$url = '';
			
			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}
				
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			$this->data['limits'] = array();
	
			$limits = array_unique(array($this->config->get('config_catalog_limit'), 25, 50, 75, 100));
			
			sort($limits);
	
			foreach($limits as $limits){
				$this->data['limits'][] = array(
					'text'  => $limits,
					'value' => $limits,
					'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&limit=' . $limits)
				);
			}
			
			$url = '';
			
			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}
				
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
	
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
			if(isset($this->request->get['color']))$url .= '&color=' . $this->request->get['color'];
			if(isset($this->request->get['sort']))$url .= '&sort=' . $this->request->get['sort'];
			if(isset($this->request->get['order']))$url .= '&order=' . $this->request->get['order'];
					
			$pagination = new Pagination();
			$pagination->total = $product_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->text = $this->language->get('text_pagination');
			$pagination->url = $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&page={page}');
		
			$this->data['pagination'] = $pagination->render();
			//var_dump($this->data['pagination']);
		
			$this->data['sort'] = $sort;
			$this->data['color'] = $color;
			$this->data['order'] = $order;
			$this->data['limit'] = $limit;
		
			$this->data['continue'] = $this->url->link('common/home');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/category.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/product/category.tpl';
			} else {
				$this->template = 'default/template/product/category.tpl';
			}
			
			$this->children = array(
				//'common/column_left',
				//'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
			);
				
			$this->response->setOutput($this->render());										
    	} else {
			$url = '';
			
			if (isset($this->request->get['path'])) {
				$url .= '&path=69';// . $this->request->get['path'];
			}
			
			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}
												
			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
				
			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}
						
			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}
						
			$this->data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_error'),
				'href'      => $this->url->link('product/category', $url),
				'separator' => $this->language->get('text_separator')
			);
				
			$this->document->setTitle($this->language->get('text_error'));

      		$this->data['heading_title'] = $this->language->get('text_error');

      		$this->data['text_error'] = $this->language->get('text_error');

      		$this->data['button_continue'] = $this->language->get('button_continue');

      		$this->data['continue'] = $this->url->link('common/home');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/error/not_found.tpl';
			} else {
				$this->template = 'default/template/error/not_found.tpl';
			}
			
			$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
			);
					
			$this->response->setOutput($this->render());
		}
  	}
	public function your_socks() {
		$this->language->load('product/category'); 
		
		if (empty($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['captcha'])) {
			$aaa =  "error_captcha";
		}
		else{
			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->hostname = $this->config->get('config_smtp_host');
			$mail->username = $this->config->get('config_smtp_username');
			$mail->password = $this->config->get('config_smtp_password');
			$mail->port = $this->config->get('config_smtp_port');
			$mail->timeout = $this->config->get('config_smtp_timeout');				
			$mail->setTo($this->config->get('config_email'));
			$mail->setFrom($this->request->post['email']);
			$mail->setSender($this->request->post['name']);
			$mail->setSubject(html_entity_decode($this->language->get('email_subject').$this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			$mail->setText("ФИО: ".strip_tags(html_entity_decode($this->request->post['name'], ENT_QUOTES, 'UTF-8'))."\nТелефон покупателя: ".strip_tags(html_entity_decode($this->request->post['phone'], ENT_QUOTES, 'UTF-8'))."\nE-mail: ".strip_tags(html_entity_decode($this->request->post['email'], ENT_QUOTES, 'UTF-8'))."\nГород: ".strip_tags(html_entity_decode($this->request->post['city'], ENT_QUOTES, 'UTF-8'))."\nНазвание категории: ".strip_tags(html_entity_decode($this->request->post['name_tov'], ENT_QUOTES, 'UTF-8')));
			$mail->send();

			$aaa = "Заявка отправлена";
		}
		$this->response->setOutput($aaa);	
	}
	public function check_price() {
		$this->language->load('product/category'); 
		
		if (empty($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['captcha'])) {
			$aaa =  "error_captcha";
		}
		else{
			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->hostname = $this->config->get('config_smtp_host');
			$mail->username = $this->config->get('config_smtp_username');
			$mail->password = $this->config->get('config_smtp_password');
			$mail->port = $this->config->get('config_smtp_port');
			$mail->timeout = $this->config->get('config_smtp_timeout');				
			$mail->setTo($this->config->get('config_email'));
			$mail->setFrom($this->request->post['email']);
			$mail->setSender($this->request->post['name']);
			$mail->setSubject(html_entity_decode($this->language->get('email_subject').$this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			$mail->setText("ФИО: ".strip_tags(html_entity_decode($this->request->post['name'], ENT_QUOTES, 'UTF-8'))."\nТелефон покупателя: ".strip_tags(html_entity_decode($this->request->post['phone'], ENT_QUOTES, 'UTF-8'))."\nE-mail: ".strip_tags(html_entity_decode($this->request->post['email'], ENT_QUOTES, 'UTF-8'))."\nГород: ".strip_tags(html_entity_decode($this->request->post['city'], ENT_QUOTES, 'UTF-8'))."\nНазвание товара: ".strip_tags(html_entity_decode($this->request->post['name_tov'], ENT_QUOTES, 'UTF-8'))."\nАртикул товара: ".strip_tags(html_entity_decode($this->request->post['sku'], ENT_QUOTES, 'UTF-8'))."\nКоличество товара: ".strip_tags(html_entity_decode($this->request->post['kilkist'], ENT_QUOTES, 'UTF-8'))."\nРазмер товара: ".strip_tags(html_entity_decode($this->request->post['size'], ENT_QUOTES, 'UTF-8'))."\n<span style='display:inline-block;'><span style='margin-top: 4px;float:left; display:block;'>Цвет товара:</span> <span style='float: left; margin-left:10px; display:block;background: ".$this->request->post['color_code']."; width:20px; height:20px; border-radius:10px; border:1px solid #999;'></span></span>"."\nНазвание цвета: ".strip_tags(html_entity_decode($this->request->post['color_name'], ENT_QUOTES, 'UTF-8')));
			$mail->send();

			$aaa = "Заявка отправлена";
		}
		$this->response->setOutput($aaa);	
	}
	
	public function captcha() {
		$this->load->library('captcha');
		
		$captcha = new Captcha();
		
		$this->session->data['captcha'] = $captcha->getCode();
		
		$captcha->showImage();
	}
}
?>
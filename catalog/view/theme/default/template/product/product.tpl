<?php echo $header; ?>


<div class="breadcrumb">
   <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
   <?php } ?>
</div>
<style>
body {
	    margin-top: -2px !important;
}
@media (max-width: 1024px) { /* это будет показано при разрешении монитора до 930 пикселей */
	.request_call {
		position: absolute;
		top: 400px;
		left: 600px;
	}
	.check_price {
		position: absolute;
		top: 600px;
		left: 575px;
	}
	.zakaz_phone {
    position: absolute;
    left: 1150px;
    top: 455px;
}
}

</style>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div class="your_socks_background"></div>
<div class="your_socks">
	<form action="" method="post" id="form_your_socks" enctype="multipart/form-data">
		<h2 style="text-align: center;font-size: 18px; text-transform: uppercase; margin-bottom: 28px;font-family: arial;color: #000;"><?php echo $text_contact; ?><span class="close_form">x</span></h2>
		<div class="content" style="">
			<div class="img_name">
				<img src="">
				<?php if($attribute_groups){ ?>
				<div class="text_vsp">
					<span id="title_vsp"></span>
					<div id="size_socks">
						<span >Размеры:</span>
						<select name="size" style="    color: #0b94e2;">
						<?php
								$koma = 0;
								foreach($attribute_groups as $key0=>$attribute_group){
									//if($attribute_group['attribute_group_id'] == '7'){
										foreach($attribute_group['attribute'] as $key => $attribute){?>
											<option value="<?php echo $attribute['name'].' / '.$attribute['text'] ?>">(<? echo $attribute['name']; ?>)/(<? echo $attribute['text']; ?>)</option>
										<? $koma++;
										}
									//}
								}
							?>
						</select>
					</div>
					
				</div>
				<?php }
				else{ ?><input type="hidden" name="size" value="Размер не выбран"><? }?>
				<div id="kolichestvo">
				<span style="float: right; font-size: 15px;">КОЛИЧЕСТВО:</span>
				<input type="text" name="kilkist" value="50">
				<span class="k_vo">шт.</span>
				</div>
				<input type="hidden" name="name_tov" >
				<input type="hidden" name="sku_product" value="<?php if($sku) echo $sku; else echo "нету артикула";  ?>">
			</div>
			<input type="hidden" name="color_tov" >
			<input type="hidden" name="opt" value="request_call">
			<div id="default_text">
				<span style="font-size: 15px; text-transform: uppercase; font-weight: bold;"><? echo $text_1; ?></span><br><span style="display: block; margin-bottom: 15px; font-size: 14px;"><? echo $text_2; ?></span>
			</div>
			<input class="stand_inp" style="" placeholder="*<?php echo $entry_name; ?>" type="text" name="name" />
			<br />
			<input class="stand_inp" placeholder="*<?php echo $entry_email; ?>" type="text" name="email" />
			<br />
			<input class="stand_inp" placeholder="*<?php echo $text_telephone; ?>" type="text" name="phone" />
			<br />
			<input class="stand_inp" placeholder="<?php echo $text_city; ?>" type="text" name="city" />
			<br />
		</div>
		<div style="margin:0 0 14px 0;    height: 38px;">
			<span class="span_captcha" ><img src="index.php?route=product/product/captcha" alt="" id="captcha" /></span>
			<div class="reload_captcha"></div>
			<input class="stand_inp" style=" float: right; margin-top: -1px;width: 200px;" placeholder="*<?php echo $entry_captcha; ?>" type="text" name="captcha" value="" />
		</div>
		<div class="buttons">
			
			<input type="button" onClick="ValidFormCheckPrice2();" value="Уточнить цену" class="button_submit " />
		</div>
		<div id="captcha_text_success2"></div>
		<div class="background_success"></div>
	</form>
</div>
<div id="content"><?php echo $content_top; ?>
  
  <div class="main-content">
  <div class="product-info">
    <?php if ($config_zoom) { ?>
	  <div class="left">
        <?php if ($images) { ?>
          <div class="image-additional">
            <?php foreach ($images as $image) { ?>
              <a href="<?php echo $image['popup']; ?>" id="this_image" style="display:none;" title="<?php echo $heading_title; ?>" class="cloud-zoom-gallery see_color" data-id="<? echo $image['color'][0]['id']; ?>" rel="useZoom: 'zoom1', smallImage: '<?php echo $image['thumb_zoom']; ?>' "><img src="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" id="image" /></a>
            <?php } ?>
          </div>
        <?php } ?>    
      </div>
	<?php } else { ?>
	  <div class="left">
		<?php/* if ($thumb) { ?>
		  <div class="image"><a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" data-gal="prettyPhoto[gallery]"><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" id="image" /></a><?php echo $sale; ?><?php echo $new; ?><?php echo $popular; ?></div>
		<?php } */?>
		<!--div class="cart">
		  <div>
		    <div class="cart-box-bottom">
			  <div class="quantity-input">
			    <span class="minus"></span>
			    <input id="quantity" type="text" name="quantity" size="2" value="<?php echo $minimum; ?>" />
			    <span class="plus"></span>
			    <input type="hidden" name="product_id" size="2" value="<?php echo $product_id; ?>" />
			  </div>
			  <a onclick="addToCompare('<?php echo $product_id; ?>');" class="poshytips button-compares" title="<?php echo $button_compare; ?>" /></a>
			  <a onclick="addToWishList('<?php echo $product_id; ?>');" class="poshytips button-wishlists" title="<?php echo $button_wishlist; ?>" /></a>
			  <input type="button" value="<?php echo $button_cart; ?>" id="button-cart" class="poshytips button-carts" title="<?php echo $button_cart; ?>" />
		    </div>
		    <?php if ($minimum > 1) { ?>
			  <div class="minimum"><?php echo $text_minimum; ?></div>
		    <?php } ?>
		  </div>
	    </div-->
		<?php if ($images) { ?>
		  <div class="image-additional">
			<?php foreach ($images as $image) { ?>
			  <a id="this_image" class="see_color" data-id="<? echo $image['color'][0]['id']; ?>"  title="<?php echo $heading_title; ?>" data-gal="prettyPhoto[gallery]"><img src="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
			<?php } ?>
		  </div>
		<?php } ?>
	  </div>
	<?php } ?>
	<div class="center">
		<?php if ($thumb) { ?>
            <div class="image"> 
              <a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" class = 'cloud-zoom' id='zoom1' rel="position: 'right' ,showTitle:false, adjustX:-100, adjustY:0, zoomWidth:620, zoomHeight:385,smoothMove:5" data-gal="prettyPhoto[gallery]"><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
             
			</div>
          <?php } elseif($images){ ?>
			<div class="image"> 
				<a  title="<?php echo $heading_title; ?>" class = 'cloud-zoom' id='zoom1' rel="position: 'right' ,showTitle:false, adjustX:-0, adjustY:-4" data-gal="prettyPhoto[gallery]"   ><img src="<?php echo $images[0]['popup']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
			</div>
			<?}?>
		<?php /*if ($images) { ?>
			<div class="image"> 
				<a  title="<?php echo $heading_title; ?>" class = 'cloud-zoom' id='zoom1' rel="position: 'right' ,showTitle:false, adjustX:-0, adjustY:-4" data-gal="prettyPhoto[gallery]"   ><img src="<?php echo $images[0]['popup']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
			</div> 
		<?php 
		}
		elseif ($thumb) { ?>
            <div class="image"> 
              <a  title="<?php echo $heading_title; ?>" class = 'cloud-zoom' id='zoom1' rel="position: 'right' ,showTitle:false, adjustX:-0, adjustY:-4" data-gal="prettyPhoto[gallery]"   ><img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a>
              
			</div>
          <?php }*/ ?>
	</div>
    <div class="right" style="float: right; margin-left: 0;    max-width: 400px;">
      <h1 style="padding-left: 0; font-size: 17px;font-weight: 600; text-transform: uppercase; margin-bottom: 10px;"><?php echo $heading_title; ?></h1>
	  <?php if($sku){ ?>
		<div class="text_color">
			<span>Артикул: </span><span><?php echo $sku ?></span>
		</div>
		<?php } ?>
		<div class="text_color">
			<span>Цвет: </span><span><?php echo $main_name_color ?></span>
		</div>
		<?if($attribute_groups){ ?>
		<div id="size_socks">
			<span>Размеры:</span>	
			<?php
				foreach($attribute_groups as $key0=>$attribute_group){
					//if($attribute_group['attribute_group_id'] == '7'){
						$koma = 0;
						foreach($attribute_group['attribute'] as $key => $attribute){?>
							<? if($koma > 0){ echo ", ";} ?><span data-value='<?php echo $attribute['name'].' / '.$attribute['text'] ?>' data-id="<? echo $key0.'_'.$key; ?>" class="socks_size"><span>(<? echo $attribute['name']; ?>)</span><span>(<? echo $attribute['text']; ?>)</span></span>
						<? $koma++;
						}
					//}
				}
			?>
		</div>
		<?php } ?>
	  <div id="tab-description" class="tab-content"><?php echo $description; ?></div>
	  <div>
	  <? if(isset($product_cat)){?>
		<span style="font-size: 17px; color: #000; display: block;">Цвет:</span><br>
		<div class="colors">
		<div style="background: <? echo $main_code_color ?>;cursor: context-menu;" class='black_border' ><a class='poshitips' style="cursor: context-menu;" title="<?php echo $main_name_color ?>"></a></div>
		<? $result_id = array();
			$result_color = array();
		
		foreach($product_cat as $result){
			if($result['code_color']){
			if($result['select']) 
			/*<div alt="<? echo $image['color'][0]['name'];?>" data-color="<? echo $image['color'][0]['code']; ?>" data-id="<? echo $image['color'][0]['id'];?>" style="background: <? echo $image['color'][0]['code']; ?>;" ></div>*/
		?>	
			<div style="background: <? echo $result['code_color'] ?>;"  ><a href="<?php echo $result['href']  ?>" class='poshitips' title="<?php echo $result['name_color'] ?>"></a></div>
		<? 	}
		}	?>
		<div id="see_color" data-color="<? echo $main_code_color ?>" data-name_color=""></div>
		</div>
		<?}?>
	  </div>
	  <? if($quantity > 1){?>
	  <div class="isset_socks have_socks">
		<div>Есть в наличии</div>
	  </div>
	   <? }else{?>
	  <div class="isset_socks not_have_socks">
		<div>Нет в наличии</div>
	  </div>
	  <?}?>
	  <br><br>
	  <div style="float:left;">
		<a class="tovar_socks_main">Уточнить цену</a>
	  </div>
    </div>
  </div>
  <?/*<div id="tabs" class="htabs"><a href="#tab-description"><?php echo $tab_description; ?></a>
    <?php if ($attribute_groups) { ?>
    <a href="#tab-attribute"><?php echo $tab_attribute; ?></a>
    <?php } ?>
    <?php if ($review_status) { ?>
    <a href="#tab-review"><?php echo $tab_review; ?></a>
    <?php } ?>
    <?php if ($products) { ?>
    <a href="#tab-related"><?php echo $tab_related; ?> (<?php echo count($products); ?>)</a>
    <?php } ?>
  </div> */?>
  

  </div>
  
  <?php echo $content_bottom; ?></div>
<script type="text/javascript"><!--
$(document).ready(function(){
	$('a.poshitips').poshytip({
		className: 'tip-twitter',
		showTimeout: 1,
		alignTo: 'target',
		alignX: 'center',
		offsetY: 5,
		allowTipHover: false,
		fade: false,
		slide: false
	});
});
$(window).load(function(){
	$('.your_socks .reload_captcha').click(function(){
		$('.your_socks #captcha').attr("src", "index.php?route=product/product/captcha/?"+new Date().getTime());
	});
	$('.right .socks_size').click(function(){
		$('.right .socks_size').each(function(){
			$(this).removeClass('select');
		});
		$(this).addClass('select');
		$("select[name='size'] [value='"+$(this).attr('data-value')+"']").attr("selected", "selected");
		//$('input[name="size"]').val($(this).text());
	});
	$('.tovar_socks_main').click(function(){
		$('.right .socks_size').each(function(){
			if($(this).hasClass('select')){
				jlkjlkjl = $(this).attr('data-id');
				$('.your_socks #size_socks .socks_size').each(function(){
					if($(this).attr('data-id') == jlkjlkjl){
						$(this).click();
					}
				});
			}
		});
		var img = $('.product-info .center').find('img').attr('src');
		var name_tov = $(this).parent().parent().find('h1').text();
		var img_color = $('.product-info .center').find('img').attr('data-color');
		$('.your_socks .img_name').find('img').attr('src', img);
		$('.your_socks').find('input[name="color_tov"]').val(img_color);
		$('.your_socks .img_name').find('#title_vsp').text(name_tov);
		$('.your_socks .img_name').find('input[name="name_tov"]').val(name_tov);
		$('.your_socks #captcha').attr("src", "index.php?route=product/product/captcha/?"+new Date().getTime());
		$('.your_socks_background, .your_socks').fadeIn();
	});
	$('#container #wrapper').on('click', '.your_socks_background, .close_form', function(){
		$('.your_socks_background, .your_socks').fadeOut();
		$('.your_socks').fadeOut();
	});
	$('#form_your_socks').find('input[name="phone"]').mask("+38(099)-999-99-99?");
	
	$('#size_socks').on('click', ".socks_size", function(){
		$('#size_socks .socks_size').each(function(){
			$(this).removeClass('select');
		});
		$(this).addClass('select');
		$('input[name="size"]').val($(this).text());
	});
	$('#container #wrapper').on('click', '.your_socks .background_success, .your_socks .close2', function(){
		$('.background_success, .your_socks #captcha_text_success2').fadeOut();
		$('.your_socks #captcha_text_success2').fadeOut();
		$('.your_socks_background, .your_socks').fadeOut();
		$('.your_socks').fadeOut();
	});
	
	$('#container #wrapper').on('keyup keydown', 'input[name="kilkist"]', function(){
		if(this.value.toString().search(/[^0-9]/) != -1)
		this.value = this.value.toString().replace( /[^0-9]/g, '');
	});
	
});
function ValidFormCheckPrice2(){
	$('.your_socks div#size_socks').css("border","1px solid #fff");
	$('.your_socks #size_socks .socks_size').css("border-bottom","1px solid #fff");
	$('#form_your_socks input').css("border", "none")
	prov = 0;
	$('.your_socks #captcha_text_success2').fadeOut();
	$('.your_socks #captcha_text_success2').removeClass('error_call');
	//$('#robot2').fadeOut();
	$('#form_your_socks input').css("border","1px solid #ccc");
	$('#form_your_socks textarea').css("border","1px solid #ccc");
	var captcha = $('#form_your_socks').find('input[name="captcha"]').val();
	var name = $('#form_your_socks').find('input[name="name"]').val();
	var phone = $('#form_your_socks').find('input[name="phone"]').val();
	//var captcha_pr = $("#form_your_socks").find('#robokop2').prop("checked");
	var city = $('#form_your_socks').find('input[name="city"]').val();
	var email = $('#form_your_socks').find('input[name="email"]').val();
	var enquiry = $('#form_your_socks').find('textarea').val();
	var kilkist = $('#form_your_socks').find('input[name="kilkist"]').val();
	var atpos = email.indexOf("@");
	var dotpos = email.lastIndexOf(".");
	
	/*if(captcha_pr == false){
		$('#robot2').fadeIn();
		prov = 1;
	}*/
	<?if($attribute_groups){ ?>var size_sock = $('#form_your_socks').find('input[name="size"]').val();
	
	if($('.your_socks #size_socks .socks_size').text() != "" && size_sock == ""){
		$('.your_socks #size_socks .socks_size').css("border-bottom","1px solid red");
		prov = 1;
	}
	<? }?>
	if(kilkist == ""){
		$('#form_your_socks').find('input[name="kilkist"]').css("border","1px solid red");
		prov = 1;
	}
	if(name == ""){
		$('#form_your_socks').find('input[name="name"]').css("border","1px solid red");
		prov = 1;
	}
	if(captcha == ""){
		$('#form_your_socks').find('input[name="captcha"]').css("border","1px solid red");
		prov = 1;
	}
	if(phone == ""){
		$('#form_your_socks').find('input[name="phone"]').css("border","1px solid red");
		prov = 1;
	}
	if (email == "" || atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
	{
		$('#form_your_socks').find('input[name="email"]').css("border", "1px solid red" );
		prov = 1;
	}
	if(enquiry == ""){
		$('#form_your_socks').find('textarea').css("border","1px solid red");
		prov = 1;
	}
	
	if(prov == 1){ return false; }
	else{
		jQuery.ajax({
			url: 'index.php?route=product/product/your_socks',
			type:     "POST", //Тип запроса
			dataType: "html", //Тип данных
			data: 	  jQuery("#"+'form_your_socks').serialize(), 
			success: function(response) { //Если все нормально
				if(response == "error_captcha"){
					$('#form_your_socks').find('input[name="captcha"]').css("border","1px solid red");
					//$('#captcha_text_success2').fadeIn();
					$('.your_socks #captcha').attr("src", "index.php?route=product/product/captcha/?"+new Date().getTime());
					$('input[name="captcha"]').val('');
				}
				else{
					$('.your_socks #captcha_text_success2').html("<div class='close2'>x</div>"+response);
					$('.your_socks #captcha_text_success2').addClass('success_call');
					$('.your_socks #captcha_text_success2').fadeIn();
					document.getElementById('form_your_socks').reset();
					$('.your_socks .background_success').fadeIn();
					$('.your_socks #captcha').attr("src", "index.php?route=product/product/captcha/?"+new Date().getTime());
					$('#size_socks .socks_size').each(function(){
						$(this).removeClass('select');
					});
				}
			},
			error: function(response) {  
				//Если ошибка
				alert("Ошибка");
				$('#captcha_text_success2').addClass('error_call');
				document.getElementById('captcha_text_success2').innerHTML = "Ошибка при отправке формы";
				$('#captcha_text_success2').fadeIn();
			}
		});
	}
}
</script>
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.center img').attr('data-color', $('#see_color').attr('data-color'));
	$('.see_color').each(function(){
		if($(this).attr('data-id') == $('#see_color').attr('data-id')){
			$(this).css("display","block");
		}
	});
	$('.image-additional img').click(function(){
		$('.center img').attr('src', $(this).attr('src'));
		$('.center a').attr('href', $(this).attr('src'));
		$('.image-additional img').each(function(){
			$(this).removeClass('yellov_border');
		});
		$(this).addClass('yellov_border');
	});  
	//$('.image-additional .cloud-zoom-gallery[data-id="5"]').click();
	/* клик на цвет */
	/*$('.colors div').click(function(){
		var color_nuw = $(this).attr('data-id');
		$('.image-additional .cloud-zoom-gallery[href="'+$('.see_color[data-id="'+color_nuw+'"] img').first().attr('src')+'"]').first().click();
		$('.colors div').each(function(){
			$(this).removeClass("black_border");
		});
		$('.image-additional .cloud-zoom-gallery[data-id="5"]').first().click();
		$(this).addClass("black_border");
		$('.center img').attr('data-color', $(this).attr('data-color'));
		
		$('.see_color').each(function(){
			if($(this).attr('data-id') == color_nuw){
				$(this).css("display","block");
			}
			else{
				$(this).css("display","none");
			}
		});
		$('.center img').attr('src', $('.see_color[data-id="'+color_nuw+'"] img').first().attr('src'));
		$('.center a').attr('href', $('.see_color[data-id="'+color_nuw+'"] img').first().attr('src'));
		old_image = $('.center img').attr('src');
	});*/
	$('.colors div').hover(
		function(){
			$(this).css("opacity", "0.5");
			old_image = $('.center img').attr('src');
			var color_nuw = $(this).attr('data-id');
			$('.center img').attr('src', $('.see_color[data-id="'+color_nuw+'"] img').first().attr('src'));
			$('.see_color[data-id="'+color_nuw+'"] img').first().addClass('yellov_border');
		},
		function(){
			$(this).css("opacity", "1");
			$('.center img').attr('src', old_image);
		}
	);
});
$('#button-cart').bind('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, information, .error').remove();
			
			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						$('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
					}
				}
			} 
			
			if (json['success']) {
				$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					
				$('.success').fadeIn('slow');
					
				$('#cart-total').html(json['total']);
				
				$('html, body').animate({ scrollTop: 0 }, 'slow'); 
			}	
		}
	});
});
//--></script>
<?php if ($options) { ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/ajaxupload.js"></script>
<?php foreach ($options as $option) { ?>
<?php if ($option['type'] == 'file') { ?>
<script type="text/javascript"><!--
new AjaxUpload('#button-option-<?php echo $option['product_option_id']; ?>', {
	action: 'index.php?route=product/product/upload',
	name: 'file',
	autoSubmit: true,
	responseType: 'json',
	onSubmit: function(file, extension) {
		$('#button-option-<?php echo $option['product_option_id']; ?>').after('<img src="catalog/view/theme/default/image/loading.gif" class="loading" style="padding-left: 5px;" />');
		$('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', true);
	},
	onComplete: function(file, json) {
		$('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', false);
		
		$('.error').remove();
		
		if (json['success']) {
			alert(json['success']);
			
			$('input[name=\'option[<?php echo $option['product_option_id']; ?>]\']').attr('value', json['file']);
		}
		
		if (json['error']) {
			$('#option-<?php echo $option['product_option_id']; ?>').after('<span class="error">' + json['error'] + '</span>');
		}
		
		$('.loading').remove();	
	}
});
//--></script>
<?php } ?>
<?php } ?>
<?php } ?>
<script type="text/javascript"><!--
$('#review .pagination a').live('click', function() {
	$('#review').fadeOut('slow');
		
	$('#review').load(this.href);
	
	$('#review').fadeIn('slow');
	
	return false;
});			

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').bind('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#button-review').attr('disabled', true);
			$('#review-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		complete: function() {
			$('#button-review').attr('disabled', false);
			$('.attention').remove();
		},
		success: function(data) {
			if (data['error']) {
				$('#review-title').after('<div class="warning">' + data['error'] + '</div>');
			}
			
			if (data['success']) {
				$('#review-title').after('<div class="success">' + data['success'] + '</div>');
								
				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').attr('checked', '');
				$('input[name=\'captcha\']').val('');
			}
		}
	});
});
//--></script> 
<script type="text/javascript"><!--
$('#tabs a').tabs();
//--></script> 
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
	if ($.browser.msie && $.browser.version == 6) {
		$('.date, .datetime, .time').bgIframe();
	}

	$('.date').datepicker({dateFormat: 'yy-mm-dd'});
	$('.datetime').datetimepicker({
		dateFormat: 'yy-mm-dd',
		timeFormat: 'h:m'
	});
	$('.time').timepicker({timeFormat: 'h:m'});
});
//--></script> 
<script type="text/javascript" ><!--
$(document).ready(function() {
    $('.minus').click(function () {
        var $input = $(this).parent().find('#quantity');
        var count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.plus').click(function () {
        var $input = $(this).parent().find('#quantity');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });
});
//--></script>
<?php echo $footer; ?>
<?php
// Text
$_['text_contact']      	= 'Уточнить цену';
$_['text_1']   				= 'Заполните форму';
$_['text_2']   				= 'для того чтобы узнать стоимость товаров!';
$_['text_city']   			= 'Город';
$_['entry_captcha']    		= 'Введите код с картинки:';
$_['text_telephone'] 		= 'Телефон';
$_['entry_name']     		= 'ФИО';
$_['entry_email']    		= 'E-Mail';

$_['text_refine']       	= 'Подкатегории';
$_['text_product']      	= 'Товары';
$_['text_error']        	= 'Категория не найдена!';
$_['text_empty']        	= 'В выбранной категории товаров нет.';
$_['text_quantity']     	= 'Количество:';
$_['text_manufacturer'] 	= 'Производитель:';
$_['text_model']        	= 'Модель:'; 
$_['text_points']       	= 'Бонусные баллы:'; 
$_['text_price']        	= 'Цена:'; 
$_['text_tax']          	= 'Без НДС:'; 
$_['text_reviews']      	= 'Отзывов: %s'; 
$_['text_compare']      	= 'Сравнения товаров (%s)'; 
$_['text_display']      	= 'Вид:';
$_['text_list']         	= 'Список';
$_['text_grid']         	= 'Таблица';
$_['text_sort']         	= 'Сортировка по:';
$_['text_default']      	= 'По умолчанию';
$_['email_subject']   		= 'Уточнение цены на сайте ';
$_['text_name_asc']     	= 'Название (А -&gt; Я)';
$_['text_name_desc']    	= 'Название (Я -&gt; А)';
$_['text_price_asc']    	= 'Цена (по возрастанию)';
$_['text_price_desc']   	= 'Цена (по убыванию)';
$_['text_rating_asc']   	= 'Рейтинг (по возрастанию)';
$_['text_rating_desc']  	= 'Рейтинг (по убыванию)';
$_['text_model_asc']    	= 'Модель (А -&gt; Я)';
$_['text_model_desc']   	= 'Модель (Я -&gt; А)';
$_['text_limit']        	= 'На странице:';
$_['text_no_description']	= 'Описания подкатегории нет.';
?>
﻿<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/stylesheet.css" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/cloud-zoom.css" />
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/prettyPhoto/prettyPhoto.css" />
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/poshytip/src/tip-twitter/tip-twitter.css" />
<?php foreach ($styles as $style) { ?>
<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
<script type="text/javascript" src="catalog/view/javascript/jquery/colorbox/jquery.colorbox-min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/common.js"></script>
<script type="text/javascript" src="catalog/view/javascript/cloud-zoom.1.0.2.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/prettyPhoto/jquery.prettyPhoto.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/poshytip/src/jquery.poshytip.min.js"></script>
<?php if ($quick_search) { ?>
<script type="text/javascript" src="catalog/view/javascript/quick_search.js"></script>
<?php } ?>
<script type="text/javascript" src="catalog/view/javascript/jquery.maskedinput.js"></script>
<script type="text/javascript" src="catalog/view/javascript/rs_slider/responsiveslides.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/rs_slider/responsiveslides.css" />
<script type="text/javascript" src="catalog/view/javascript/bx_slider/jquery.bxslider.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/bx_slider/jquery.bxslider.css" />
<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>
<!--[if IE 7]> 
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/ie7.css" />
<![endif]-->
<?php if ($stores) { ?>
<script type="text/javascript"><!--
$(document).ready(function() {
<?php foreach ($stores as $store) { ?>
$('body').prepend('<iframe src="<?php echo $store; ?>" style="display: none;"></iframe>');
<?php } ?>
});
//--></script>
<?php } ?>
<?php echo $google_analytics; ?>
<? //if ($_SERVER["REQUEST_URI"] == "/about_company"){ ?>
<script>
	$(document).ready(function(){
		
		var height = $('#wrapper').height()*0.7;
		var height2 = $(window).height();
		var height3 = $('#wrapper').height()<? if($_SERVER["REQUEST_URI"] == '' || $_SERVER["REQUEST_URI"] == '/' || $_SERVER["REQUEST_URI"] == '/index.php') echo ' + 510' ?>;
		var height4 = $(window).height()-210;
		if(height > height2){
			$('#top-bg').css('margin-bottom','0px');
		}
		if(height3 < height4){
			$('#wrapper').css('margin-bottom','0px');
		}
	});
	$(window).resize(function(){
		var height = $('#wrapper').height()*0.7;
		var height2 = $(window).height();
		var height3 = $('#wrapper').height()<? if($_SERVER["REQUEST_URI"] == '' || $_SERVER["REQUEST_URI"] == '/' || $_SERVER["REQUEST_URI"] == '/index.php') echo ' + 510' ?>;
		var height4 = $(window).height()-210;
		console.log(height,height2,height3,height4);
		if(height > height2){
			$('#top-bg').css('margin-bottom','0px');
		}
		else{
			//$('#top-bg').css('margin-bottom','-50px');
		}
		if(height3 < height4){
			$('#wrapper').css('margin-bottom','0px');
		}
		else{
			//$('#wrapper').css('margin-bottom','76px');
		}
	});
</script>
<? //} ?>
</head>
<body>
<? echo $yandex_metrika; ?>
<div id="top-bg">
  <div id="container">
    <div id="header">
	  <?/*<div class="links">
	    <a href="<?php echo $shopping_cart; ?>"><?php echo $text_shopping_cart; ?></a>
		<a href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a>
	  </div>*/?>
	  <?php if ($informations) { ?>
	    <!--div id="information" class="dropdown"><a class="info-link"><span class="dropdown-link"><?php echo $text_information; ?></span></a>
		  <div class="dropdown-block">
			<?php foreach ($informations as $information) { ?>
			  <a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a>
			<?php } ?>
		  </div>
		</div-->
	  <?php } ?>
	  <?/*php if ($manufacturer_top_menu == 'top_menu') { ?>
		<div id="manufacturer" class="dropdown"><a class="info-link"><span class="dropdown-link"><?php echo $text_manufacturers; ?></span></a>
		  <div class="dropdown-block">
			<?php foreach ($manufacturers as $manufacturer) { ?>
			  <?php if ($manufacturer_image) { ?>
				<a class="manufacturer-image" href="<?php echo $manufacturer['href']; ?>"><img align="absmiddle" src="<?php echo $manufacturer['image']; ?>"><?php echo $manufacturer['name']; ?></a>
			  <?php } else { ?>
				<a href="<?php echo $manufacturer['href']; ?>"><?php echo $manufacturer['name']; ?></a>
			  <?php } ?>
			<?php } ?>
		  </div>
		</div>
	  <?php } */?>
	 
	  
	  <?/*php if (($contacts_display == 'header') || ($contacts_display == 'header_footer')) { ?>
		<div id="contact" class="dropdown"><a class="info-link"><span class="dropdown-link"><?php echo $text_contact; ?></span></a>
			
		  <div class="dropdown-block">
		    <?php if ($contacts_address) { ?>
			  <div class="contact"><?php echo $text_address; ?><span class="contact-data"><?php echo $contacts_address; ?></span></div>
			<?php } ?>
			<?php if ($contacts_email) { ?>
			  <div class="contact"><?php echo $text_email_address; ?><span class="contact-data"><?php echo $contacts_email; ?></span></div>
			<?php }  ?>
			<?php if ($contacts_telephone) { ?>
			  <div class="contact"><?php echo $text_telephone; ?><span class="contact-data"><?php echo $contacts_telephone; ?></span></div>
			<?php } ?>
			<?php if ($contacts_mobile_telephone) { ?>
			  <div class="contact"><?php echo $text_mobile_telephone; ?><span class="contact-data"><?php echo $contacts_mobile_telephone; ?></span></div>
			<?php } ?>
			<?php if ($contacts_fax) { ?>
			  <div class="contact"><?php echo $text_fax; ?><span class="contact-data"><?php echo $contacts_fax; ?></span></div>
			<?php } ?>
		  </div>
		</div>
	  <?php } */?>
	  <?php echo $language; ?>
	  <?php echo $currency; ?>
	  <div class="main_header">
	  <?php if ($logo) { ?>
	    <div id="logo"><a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" /></a></div>
	  <?php } ?>
	  <?php //echo $cart; ?>
		<div id="phone">
			<?php if ($contacts_telephone) { ?>
				<div class="contact" style="float:left;"><a href="tel:<?php if($contacts_telephone_href) echo $contacts_telephone_href; ?>" class="contact-data"><?php echo $contacts_telephone; ?></a></div>
			<?php } ?>
			<?php if ($contacts_mobile_telephone) { ?>
				<div class="contact" style="float:left;"><a href="tel:<?php if($contacts_mobile_telephone_href) echo $contacts_mobile_telephone_href; ?>" class="contact-data"><?php echo $contacts_mobile_telephone; ?></a></div>
			<?php } ?>
		</div>
		<div id="search">
		<div class="button-search"></div>
		<input type="text" name="search" placeholder="<?php echo $text_search; ?>" value="<?php echo $search; ?>" />
	  </div>
	  </div>
	  <?/*<div id="welcome">
		<?php if (!$logged) { ?>
		  <div class="dropdown-login"><span><?php echo $text_login; ?></span></div>
		  <div class="dropdown-box">
			<div class="header-login-box"><?php echo $text_welcome_user; ?></div>
			<div class="content-login-box">
			  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
				<input type="text" name="email" placeholder="<?php echo $text_email; ?>" value="<?php echo $email; ?>" />
				<input type="password" name="password"  placeholder="<?php echo $text_password; ?>" value="<?php echo $password; ?>" />
				<input type="submit" value="<?php echo $button_login; ?>" class="button" /><br /><br />
				<a class="forgotten" href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
				<?php if ($redirect) { ?>
				  <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
				<?php } ?>
			  </form>
			</div>
		  </div>
		  <div class="register"><a href="<?php echo $register; ?>"><?php echo $text_register; ?><a/></div>
		<?php } else { ?>
		  <div class="dropdown">
		    <span class="dropdown-account"><?php echo $text_account; ?></span>
			<div class="dropdown-box">
			  <div class="content-account-box">
			    <ul>
				  <li><a class="welcome-user" href="<?php echo $account; ?>"><?php echo $text_logged_user; ?></a></li>
				  <li><a class="cart" href="<?php echo $shopping_cart; ?>"><?php echo $text_shopping_cart; ?></a></li>
				  <li><a class="checkout" href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a></li>
				  <li><a class="wishlist" href="<?php echo $wishlist; ?>" id="wishlist-total"><?php echo $text_wishlist; ?></a></li>
				  <li><a class="compare" href="<?php echo $compare; ?>" id="compare-total-header"><?php echo $text_compare; ?></a></li>
				  <li><a class="history" href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
				  <li><a class="download" href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>
				  <li><a class="edit" href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
				 </ul>
			  </div>
			</div>
		  </div>
		  <div class="logout"><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?><a/></div>
		<?php } ?>
	  </div>*/?>
    </div>
    <?php //if ($categories) { 	?>
      <div id="menu">
		<ul>
		  <li style="margin-left: 50px;"><a class="home" href="<?php echo $home; ?>">Главная</a></li>
		  <?php /*if ($all_news_top_menu == 'top_left') { ?>
			<li><a href="<?php echo $all_news; ?>"><?php echo $text_all_news; ?></a></li>
		  <?php }*/ ?>
			<li class="dropdown"><a <? if($about_company){ ?>href="<? echo $about_company['href']; ?>"<? } ?>><? if($about_company){ ?><? echo $about_company['title']; ?><? } ?></a>
				<!--div class="dropdown-block">
					<ul>
					<? if($about_company){ ?>
						<li class="dropdown-level"> 
							<a href="<? echo $about_company['href']; ?>"><? echo $about_company['title']; ?></a>
						</li>
					<? } ?>
					<? if($industries){ ?>
						<li class="dropdown-level"> 
							<a href="<? echo $industries['href']; ?>"><? echo $industries['title']; ?></a>
						</li>
						<? } ?>
					</ul>
				</div-->
			</li>
			<li class="dropdown"><a href="/?route=product/category&path=69">Каталог товаров</a>
				<div class="dropdown-block">
					<ul>
						<? foreach($categories2 as $category2){?>
						<li class="dropdown-level"> 
							<a href="<? echo $category2['href']; ?>"><? echo $category2['name']; ?></a>
						</li>
					<?}?>
					</ul>
				</div>
			</li>
			<? if($kak_zakaz){ ?><li class="dropdown"><a href="<? echo $kak_zakaz['href']; ?>"><? echo $kak_zakaz['title']; ?></a></li><?}?>
			<li class="dropdown"><a href="/?route=news/all_news">Статьи</a></li>
			<li class="dropdown"><a >Сотрудничество</a>
				<div class="dropdown-block">
					<ul>
						<? if($optovikam){ ?>
						<li class="dropdown-level"> 
							<a href="<? echo $optovikam['href']; ?>"><? echo $optovikam['title']; ?></a>
						</li>
						<? } ?>
						<? if($vakansii){ ?>
						<li class="dropdown-level"> 
							<a href="<? echo $vakansii['href']; ?>"><? echo $vakansii['title']; ?></a>
						</li>
						<? } ?>
						<? if($dostavka){ ?>
						<li class="dropdown-level"> 
							<a href="<? echo $dostavka['href']; ?>"><? echo $dostavka['title']; ?></a>
						</li>
						<? } ?>
						<? if($zakupki){ ?>
						<li class="dropdown-level"> 
							<a href="<? echo $zakupki['href']; ?>"><? echo $zakupki['title']; ?></a>
						</li>
						<? } ?>
					</ul>
				</div>
			</li>
			<li class="dropdown"><a href="/?route=information/contact">Контакты</a></li>
		  <!--div style="display:none;"><?php foreach ($categories as $category) { ?>
			<li class="dropdown"><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
			<?php if ($category['children']) { ?>
			  <div class="dropdown-block">
				<?php for ($i = 0; $i < count($category['children']);) { ?>
				  <ul>
					<?php $j = $i + ceil(count($category['children']) / $category['column']); ?>
					<?php for (; $i < $j; $i++) { ?>
					  <?php if (isset($category['children'][$i])) { ?>
						<li class="dropdown-level"> 
						  <?php $levels_2 = $this->model_catalog_category->getCategories($category['children'][$i]['category_id']); ?>
						  <?php if($levels_2) {  ?>
							<a href="<?php echo $category['children'][$i]['href']; ?>"><?php echo $category['children'][$i]['name']; ?><span class="parent"></span></a>
							<div class="dropdown-block-level">
							  <ul>
								<?php foreach ($levels_2 as $level_2) { ?>
								  <li><a href="<?php echo $this->url->link('product/category', 'path='.$category['category_id'].'_' . $category['children'][$i]['category_id'] . '_' . $level_2['category_id']); ?>"><?php echo $level_2['name']; ?></a></li>	
								<?php } ?>
							  </ul>
							</div>
						  <?php } else { ?>
							<a href="<?php echo $category['children'][$i]['href']; ?>"><?php echo $category['children'][$i]['name']; ?></a>
						  <?php } ?>
						</li>
					  <?php } ?>
					<?php } ?>
				  </ul>
				<?php } ?>
			  </div>
			<?php } ?>
		    </li>
		  <?php } ?></div-->
		  <?php echo $menu; ?>
		  <?php if ($all_news_top_menu == 'top_right') { ?>
			<li><a href="<?php echo $all_news; ?>"><?php echo $text_all_news; ?></a></li>
		  <?php } ?>
		</ul>
	  </div>
	<?php //} ?>
	<div class="clear"></div>
	<div id="notification"></div>
	
	<? /* toptap/ */
	if ($_SERVER["REQUEST_URI"] == "/" OR $_SERVER["REQUEST_URI"] == "/index.php?route=common/home") { 
		echo $slideshow;
		?>
		<style>
.check_price .content input {
    width: 92.5%;
   
}	
.request_call .content input {
    width: 92.5%;
   
}
textarea.stand_inp {
	 width: 92.5% !important;
}
</style>
		<?
	}
	else{?>
	<style>
@media (max-width: 1024px) {
#wrapper {
	margin-left:75px;
}
}
</style>
	<?}
	?>
	<? echo $request_call; ?>
	<div class="zakaz_phone">
	<a id="request_call" ></a>
	
	</div>
	<div id="wrapper">
	
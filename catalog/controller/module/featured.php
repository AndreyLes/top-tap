<?php
class ControllerModuleFeatured extends Controller {
	protected function index($setting) {
		//var_dump($setting);
		$this->language->load('module/featured'); 

      	$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['setting'] = $setting;
		
		$this->document->addScript('catalog/view/javascript/jquery/tabs.js');
		$this->document->addScript('catalog/view/javascript/jquery/colorbox/jquery.colorbox-min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/colorbox/colorbox.css');
		
		$this->data['admin_mail'] = $this->config->get('config_email');
		
		$this->data['button_quick_view'] = $this->language->get('button_quick_view');
		$this->data['button_cart'] = $this->language->get('button_cart');
		$this->data['text_contact'] = $this->language->get('text_contact');
		$this->data['text_telephone'] = $this->language->get('text_telephone');
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_email'] = $this->language->get('entry_email');
		$this->data['entry_enquiry'] = $this->language->get('entry_enquiry');
		$this->data['button_continue'] = $this->language->get('button_continue');
		$this->data['button_wishlist'] = $this->language->get('button_wishlist');
		$this->data['button_compare'] = $this->language->get('button_compare'); 
		
		$this->data['text_city'] = $this->language->get('text_city');
		$this->data['text_1'] = $this->language->get('text_1');
		$this->data['text_2'] = $this->language->get('text_2');
		$this->data['entry_captcha'] = $this->language->get('entry_captcha');
		
		$this->data['quick_view_featured'] = $this->config->get('config_quick_view_featured');
		
		$this->data['top_bottom'] = $setting['position'] == 'content_top' || $setting['position'] == 'content_bottom';
		$this->data['side_left'] = $setting['position'] == 'column_left';
		$this->data['side_right'] = $setting['position'] == 'column_right';
		$this->data['side'] = $setting['position'] == 'column_left' || $setting['position'] == 'column_right';
		
		if ($this->config->get('config_limit_days_new_product')) {
			$limit_days_new_product = $this->config->get('config_limit_days_new_product');
		} else {
			$limit_days_new_product = 31;
		}
		
		if ($this->config->get('config_limit_viewed_popular_product')) {
			$limit_viewed_popular_product = $this->config->get('config_limit_viewed_popular_product');
		} else {
			$limit_viewed_popular_product = 50;
		}
		
		$this->load->model('catalog/product'); 
		
		$this->load->model('tool/image');

		$this->data['products'] = array();

		$products = explode(',', $this->config->get('featured_product'));		

		if (empty($setting['limit'])) {
			$setting['limit'] = 5;
		}
		
		$products = array_slice($products, 0, (int)$setting['limit']);
		
		$timestamp 			= time();
		$date_time_array 	= getdate($timestamp);
		$hours 				= $date_time_array['hours'];
		$minutes 			= $date_time_array['minutes'];
		$seconds 			= $date_time_array['seconds'];
		$month 				= $date_time_array['mon'];
		$day 				= $date_time_array['mday'];
		$year 				= $date_time_array['year'];

		$timestamp = mktime($hours, $minutes, $seconds, $month,$day - $limit_days_new_product, $year);
		
		foreach ($products as $product_id) {
			$product_info = $this->model_catalog_product->getProduct($product_id);
			
			
			$result_image = array();
			$result_image = $this->model_catalog_product->getCategories($product_info['product_id']);
			if($result_image)
			$category_parent = $this->model_catalog_product->getCategoriesParent($result_image[0]['category_id']);
			else $category_parent = '';
			//echo $result_image[0]['category_id']."<br>".$category_parent[0]['parent_id']."<br>";
				
			if ($product_info) {
				if ($product_info['image']) {
					$image = $this->model_tool_image->resize($product_info['image'], $setting['image_width'], $setting['image_height']);
				} else {
					$image = false;
				}
				
				$results_img = $this->model_catalog_product->getProductImages($product_id);
				if(isset($results_img) && isset($results_img[0]))
				$images_prod = $this->model_catalog_product->getColorImages($results_img[0]['color_id']);
				
				if(isset($images_prod) && isset($images_prod[0])){ 
					$code_color = $images_prod[0]['code'];
					$name_color = $images_prod[0]['name'];
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}
						
				if ((float)$product_info['special']) {
					$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}
				
				if ($this->config->get('config_review_status')) {
					$rating = $product_info['rating'];
				} else {
					$rating = false;
				}
				
				if ($this->config->get('config_sticker_special_module_featured')) {
					if ((float)$product_info['special']) {
						if ($product_info['price'] > 0) {
							$sale = '<div class="stiker-special-module">' . '-' . round((($product_info['price'] - $product_info['special'])/$product_info['price'])*100, 0) . '<span>%</span>' . '</div>';
						} else {
							$sale = false;
						}	
					} else {
						$sale = false;
					}			
				} else {
					$sale = false;
				}
				
				
				if ($this->config->get('config_sticker_new_module_featured')) {
					if (($product_info['date_available']) > strftime('%Y-%m-%d',$timestamp)) {
						$new = '<div class="stiker-new-module"></div>';
					} else {
						$new = false;
					}
				} else {
					$new = false;
				}
				
				if ($this->config->get('config_sticker_popular_module_featured')) {
					if (($product_info['viewed']) > ($limit_viewed_popular_product)) {
						$popular = '<div class="stiker-popular-module"></div>';
					} else {
						$popular = false;
					}
				} else {
					$popular = false;
				}
					
				$this->data['products'][] = array(
					'attribute_groups' => $this->model_catalog_product->getProductAttributes($product_id),
					'product_id'  => $product_info['product_id'],
					'code_color'   	  => $code_color,
					'name_color'   	  => $name_color,
					'thumb'   	  => $image,
					'no_image'    => $this->model_tool_image->resize('no_image.jpg', $setting['image_width'], $setting['image_height']),
					'name'    	  => $product_info['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_grid_description_limit')) . '...',
					'price'   	  => $price,
					'special' 	  => $special,
					'rating'      => $rating,
					'sale' 	  	  => $sale,
					'sku'		  => $product_info['sku'],
					'new'     	  => $new,
					'popular'     => $popular,
					'quantity'     => $product_info['quantity'],
					'reviews'     => sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']),
					'href'    	  => $this->url->link('product/product', 'path='.$category_parent[0]['parent_id'] . '_' . $result_image[0]['category_id'] . '&product_id=' . $product_info['product_id'])
				);
			}
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/featured.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/featured.tpl';
		} else {
			$this->template = 'default/template/module/featured.tpl';
		}

		$this->render();
	}
	public function check_price() {
		$this->language->load('module/featured'); 
		
		if (empty($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['captcha'])) {
			$aaa =  "error_captcha";
		}
		else{
			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->hostname = $this->config->get('config_smtp_host');
			$mail->username = $this->config->get('config_smtp_username');
			$mail->password = $this->config->get('config_smtp_password');
			$mail->port = $this->config->get('config_smtp_port');
			$mail->timeout = $this->config->get('config_smtp_timeout');				
			$mail->setTo($this->config->get('config_email'));
			$mail->setFrom($this->request->post['email']);
			$mail->setSender($this->request->post['name']);
			$mail->setSubject(html_entity_decode($this->language->get('email_subject').$this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			$mail->setText("ФИО: ".strip_tags(html_entity_decode($this->request->post['name'], ENT_QUOTES, 'UTF-8'))."\nТелефон покупателя: ".strip_tags(html_entity_decode($this->request->post['phone'], ENT_QUOTES, 'UTF-8'))."\nE-mail: ".strip_tags(html_entity_decode($this->request->post['email'], ENT_QUOTES, 'UTF-8'))."\nГород: ".strip_tags(html_entity_decode($this->request->post['city'], ENT_QUOTES, 'UTF-8'))."\nНазвание товара: ".strip_tags(html_entity_decode($this->request->post['name_tov'], ENT_QUOTES, 'UTF-8'))."\nАртикул товара: ".strip_tags(html_entity_decode($this->request->post['sku'], ENT_QUOTES, 'UTF-8'))."\nКоличество товара: ".strip_tags(html_entity_decode($this->request->post['kilkist'], ENT_QUOTES, 'UTF-8'))."\nРазмер товара: ".strip_tags(html_entity_decode($this->request->post['size'], ENT_QUOTES, 'UTF-8'))."\n<span style='display:inline-block;'><span style='margin-top: 4px;float:left; display:block;'>Цвет товара:</span> <span style='float: left; margin-left:10px; display:block;background: ".$this->request->post['color_code']."; width:20px; height:20px; border-radius:10px; border:1px solid #999;'></span></span>"."\nНазвание цвета: ".strip_tags(html_entity_decode($this->request->post['color_name'], ENT_QUOTES, 'UTF-8')));
			$mail->send();

			$aaa = "Заявка отправлена";
		}
		$this->response->setOutput($aaa);	
	}
	
	public function captcha() {
		$this->load->library('captcha');
		
		$captcha = new Captcha();
		
		$this->session->data['captcha'] = $captcha->getCode();
		
		$captcha->showImage();
	}
}
?>
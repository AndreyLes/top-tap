<script>
$(document).ready(function(){
	$('#request_call').click(function(){
		$('.request_call #captcha').attr("src", "index.php?route=module/request_call/captcha/?"+new Date().getTime());
		$('.request_call_background, .request_call').fadeIn();
	});
	$('#container').on('click', '.request_call_background, .close_form', function(){
		$('.request_call_background, .request_call').fadeOut();
	});
	$('#form_request_call').find('input[name="phone"]').mask("+38(099)-999-99-99?");
	$('body').on('click', '.request_call .background_success, .request_call .close2', function(){
	
		$('.request_call .background_success, .request_call #captcha_text_success').fadeOut();
		$('.request_call #captcha_text_success').fadeOut();
		$('.request_call_background, .request_call').fadeOut();
	});
	$('.request_call .reload_captcha').click(function(){
		$('.request_call #captcha').attr("src", "index.php?route=module/request_call/captcha/?"+new Date().getTime());
	});
});
function ValidFormRequestCall(){
	$('#form_request_call input').css("border", "none")
	prov = 0;
	$('#captcha_text_success').fadeOut();
	$('#captcha_text_success').removeClass('error_call');
	
	$('#form_request_call input').css("border","1px solid #ccc");
	$('#form_request_call textarea').css("border","1px solid #ccc");
	
	var name = $('#form_request_call').find('input[name="name"]').val();
	var phone = $('#form_request_call').find('input[name="phone"]').val();
	var captcha = $('#form_request_call').find('input[name="captcha"]').val();
	//var captcha_pr = $("#form_request_call").find('#robokop').prop("checked");
	var email = $('#form_request_call').find('input[name="email"]').val();
	//var enquiry = $('#form_request_call').find('textarea').val();
	var atpos = email.indexOf("@");
	var dotpos = email.lastIndexOf(".");
	
	
	if(name == ""){
		$('#form_request_call').find('input[name="name"]').css("border","1px solid red");
		prov = 1;
	}
	if(captcha == ""){
		$('#form_request_call').find('input[name="captcha"]').css("border","1px solid red");
		prov = 1;
	}
	if(phone == ""){
		$('#form_request_call').find('input[name="phone"]').css("border","1px solid red");
		prov = 1;
	}
	if (email == "" || atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
	{
		$('#form_request_call').find('input[name="email"]').css("border", "1px solid red" );
		prov = 1;
	}
	/*if(enquiry == ""){
		$('#form_request_call').find('textarea').css("border","1px solid red");
		prov = 1;
	}*/
	
	if(prov == 1){ return false; }
	else{
		jQuery.ajax({
			url: 		'index.php?route=module/request_call/request_call',
			type:     "POST", 
			dataType: "html", 
			data: 	  jQuery("#"+'form_request_call').serialize(), 
			
			success: function(response) { //Если все нормально
				if(response == "error_captcha"){
					$('#form_request_call').find('input[name="captcha"]').css("border","1px solid red");
					$('#captcha').attr("src", "index.php?route=module/request_call/captcha/?"+new Date().getTime());
					$('input[name="captcha"]').val('');
				}
				else{
					$('#captcha_text_success').html("<div class='close2'>x</div>"+response);
					$('#captcha_text_success').addClass('success_call');
					$('#captcha_text_success').fadeIn();
					document.getElementById('form_request_call').reset();
					$('.request_call .background_success').fadeIn();
					$('#captcha').attr("src", "index.php?route=module/request_call/captcha/?"+new Date().getTime());
				}
				
			},
			error: function(response) {  
				//Если ошибка
				$('#captcha_text_success').addClass('error_call');
				document.getElementById('captcha_text_success').innerHTML = "Ошибка при отправке формы";
				$('#captcha_text_success').fadeIn();
			}
		});
	}
}
</script>
<style>
	::-webkit-input-placeholder {color:#999;}
	::-moz-placeholder          {color:#999;}/* Firefox 19+ */
	:-moz-placeholder           {color:#999;}/* Firefox 18- */
	:-ms-input-placeholder      {color:#999;}
</style>
<div class="request_call_background"></div>
<div class="request_call">
	<form action="" method="post" id="form_request_call" enctype="multipart/form-data">
		<h2 style="text-align: center; text-transform:uppercase; font-size: 23px;margin-bottom: 12px;font-family: arial;color: #000;"><?php echo $text_contact; ?><span class="close_form">x</span></h2>
		<div class="content">
			<input type="hidden" name="opt" value="request_call">
			<input class="stand_inp" style="<?php if ($error_name) { ?> border:1px solid red; <?php } ?>" placeholder="*<?php echo $entry_name; ?>" type="text" name="name" value="<?php echo $name; ?>" />
			<br />
			<input class="stand_inp" style="<?php if ($error_email) { ?> border:1px solid red; <?php } ?>" placeholder="*<?php echo $entry_email; ?>" type="text" name="email" value="<?php echo $email; ?>" />
			<br />
			<input class="stand_inp" style="<?php if ($error_phone) { ?> border:1px solid red; <?php } ?>" placeholder="*<?php echo $text_telephone; ?>" type="text" name="phone" value="<?php echo $phone; ?>" />
			<br />
			
			<textarea class="stand_inp" placeholder="<?php echo $entry_enquiry; ?>" name="enquiry" cols="40" rows="7" style="width: 92.5%; resize: none;border: 1px solid #fff; <?php if ($error_enquiry) { ?> border:1px solid red; <?php } ?>"><?php echo $enquiry; ?></textarea>
			<br />
			<?/*<b><?php echo $entry_captcha; ?></b><br />
			<input type="text" name="captcha" value="<?php echo $captcha; ?>" />
			<br />
			<img src="index.php?route=information/contact/captcha" alt="" />
			<?php if ($error_captcha) { ?>
			<span class="error"><?php echo $error_captcha; ?></span>
			<?php } ?> */?>
		</div>
		<div style="margin: 14px 0;    height: 38px;">
			<span class="span_captcha" ><img src="index.php?route=module/request_call/captcha" alt="" id="captcha" /></span>
			<div class="reload_captcha"></div>
			<input class="stand_inp" style=" float: right; margin-top: -1px;width: 200px;" placeholder="*<?php echo $entry_captcha; ?>" type="text" name="captcha" value="" />
		</div>
		<div class="buttons">
			<input type="button" onClick="ValidFormRequestCall();" value="<?php echo $button_continue; ?>" class="button_submit" />
		</div>
		<div id="captcha_text_success"></div>
		<div class="background_success"></div>
	</form>
</div>
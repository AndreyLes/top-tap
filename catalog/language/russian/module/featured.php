<?php
// Heading 
$_['heading_title'] = 'Топ недели';
$_['text_telephone'] 		= 'Телефон';
$_['entry_name']     		= 'ФИО';
$_['entry_email']    		= 'E-Mail';
$_['entry_enquiry']  		= 'Комментарий';
$_['button_continue'] 		= 'Отправить';
$_['text_contact']   		= 'Уточнить цену';
$_['email_subject']   		= 'Уточнение цены на сайте ';
$_['text_1']   				= 'Заполните форму';
$_['text_2']   				= 'для того чтобы узнать стоимость товаров!';
$_['text_city']   			= 'Город';
$_['entry_captcha']     = 'Введите код с картинки:';

// Text
$_['text_reviews']  = 'Отзывов: %s'; 
?>
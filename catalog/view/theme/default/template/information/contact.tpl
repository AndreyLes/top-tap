<?php echo $header; ?>
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="main-content">
	<div class="contact-info">
    <div class="content">
	  <div class="left">
		<?php if ($address) { ?>
		<span class="title_groupe"><?php echo $text_address; ?></span><br />
        <span style="text-transform:uppercase;" class="content_groupe"><?php echo $address; ?></span>
		<br />
		<?php } ?>
	  </div>
	  <div class="center">
		<?php if ($telephone) { ?>
        <span class="title_groupe"><?php echo $text_telephone; ?></span><br />
        <span class="content_groupe"><a href="tel:<?php if($telephone_href) echo $telephone_href; ?>" class="contact-data"><?php echo $telephone; ?></a></span><br />
		<?php } ?>
		<?php if ($mobi_telephone) { ?>
        <span class="content_groupe"><a href="tel:<?php if($mobi_telephone_href) echo $mobi_telephone_href; ?>" class="contact-data"><?php echo $mobi_telephone; ?></a></span><br />
        <?php } ?>
	  </div>
      <div class="right">
       	<?php if ($store_email) { ?>
		<span class="title_groupe"><?php echo $text_email; ?></span><br />
        <span class="content_groupe"><a class="href_mail" href="mailto:<?php echo $store_email; ?>"><?php echo $store_email; ?></a></span>
		<?php } ?>
      </div>
    </div>
	<div class="maps"><?php if($iframe_map) echo $iframe_map; ?></div>
    </div>
    <!--h2><?php echo $text_contact; ?></h2>
    <div class="content">
    <b><?php echo $entry_name; ?></b><br />
    <input type="text" name="name" value="<?php echo $name; ?>" />
    <br />
    <?php if ($error_name) { ?>
    <span class="error"><?php echo $error_name; ?></span>
    <?php } ?>
    <br />
    <b><?php echo $entry_email; ?></b><br />
    <input type="text" name="email" value="<?php echo $email; ?>" />
    <br />
    <?php if ($error_email) { ?>
    <span class="error"><?php echo $error_email; ?></span>
    <?php } ?>
    <br />
    <b><?php echo $entry_enquiry; ?></b><br />
    <textarea name="enquiry" cols="40" rows="10" style="width: 99%;"><?php echo $enquiry; ?></textarea>
    <br />
    <?php if ($error_enquiry) { ?>
    <span class="error"><?php echo $error_enquiry; ?></span>
    <?php } ?>
    <br />
    <b><?php echo $entry_captcha; ?></b><br />
    <input type="text" name="captcha" value="<?php echo $captcha; ?>" />
    <br />
    <img src="index.php?route=information/contact/captcha" alt="" />
    <?php if ($error_captcha) { ?>
    <span class="error"><?php echo $error_captcha; ?></span>
    <?php } ?>
    </div>
    <div class="buttons">
      <div class="right"><input type="submit" value="<?php echo $button_continue; ?>" class="button" /></div>
    </div-->
  
  </div>
 
  <?php echo $content_bottom; ?></div>
<?php echo $footer; ?>
<style>	
	
</style>
<script>
$(window).load(function(){
	$('.tovar_socks').click(function(){
		var img = $(this).parent().parent().parent().find('.image img').attr('src');
		var name_tov = $(this).parent().parent().parent().find('.image img').attr('alt');
		var size_socks = $(this).parent().parent().find('.size_socks').html();
		$('.check_price .img_name').find('input[name="color_code"]').val($(this).attr('data-color_code'));
		$('.check_price .img_name').find('input[name="color_name"]').val($(this).attr('data-color_name'));
		$('.check_price .img_name').find('input[name="sku"]').val($(this).attr('data-article'));
		$('.check_price #size_socks').html(size_socks);
		$('.check_price .img_name').find('img').attr('src', img);
		$('.check_price .img_name').find('#title_vsp').text(name_tov);
		$('.check_price .img_name').find('input[name="name_tov"]').val(name_tov);
		$('.check_price #captcha').attr("src", "index.php?route=module/featured/captcha/?"+new Date().getTime());
		$('.check_price_background, .check_price').fadeIn();
	});
	$('#container #wrapper').on('click', '.check_price_background, .close_form', function(){
		$('.check_price_background, .check_price').fadeOut();
		$('.check_price').fadeOut();
	});
	$('#form_check_price').find('input[name="phone"]').mask("+38(099)-999-99-99?");
	
	$('.check_price #size_socks').on('click', ".socks_size", function(){
		$('.check_price #size_socks .socks_size').each(function(){
			$(this).removeClass('select');
		});
		$(this).addClass('select');
		$('input[name="size"]').val($(this).text());
	});
	
	$('#container #wrapper').on('click', '.check_price .background_success, .check_price .close2', function(){
		$('.background_success, .check_price #captcha_text_success3').fadeOut();
		$('.check_price #captcha_text_success3').fadeOut();
		$('.check_price_background, .check_price').fadeOut();
		$('.check_price').fadeOut();
	});
	$('#container #wrapper').on('keyup keydown', 'input[name="kilkist"]', function(){
		if(this.value.toString().search(/[^0-9]/) != -1)
		this.value = this.value.toString().replace( /[^0-9]/g, '');
	});
	$('.check_price .reload_captcha').click(function(){
		$('.check_price #captcha').attr("src", "index.php?route=module/featured/captcha/?"+new Date().getTime());
	});
});
function ValidFormCheckPrice(){
	$('.check_price #size_socks .socks_size').css("border-bottom","1px solid #fff");
	$('.check_price div#size_socks').css("border","1px solid #fff");
	$('#form_check_price input').css("border", "none")
	prov = 0;
	$('.check_price #captcha_text_success3').fadeOut();
	$('.check_price #captcha_text_success3').removeClass('error_call');
	//$('#robot2').fadeOut();
	$('#form_check_price input').css("border","1px solid #ccc");
	$('#form_check_price textarea').css("border","1px solid #ccc");
	var captcha = $('#form_check_price').find('input[name="captcha"]').val();
	var name = $('#form_check_price').find('input[name="name"]').val();
	var phone = $('#form_check_price').find('input[name="phone"]').val();
	//var captcha_pr = $("#form_check_price").find('#robokop2').prop("checked");
	var kilkist = $('#form_check_price').find('input[name="kilkist"]').val();
	var city = $('#form_check_price').find('input[name="city"]').val();
	var email = $('#form_check_price').find('input[name="email"]').val();
	var enquiry = $('#form_check_price').find('textarea').val();
	var atpos = email.indexOf("@");
	var dotpos = email.lastIndexOf(".");
	
	/*if(captcha_pr == false){
		$('#robot2').fadeIn();
		prov = 1;
	}*/
	/*var size_sock = $('#form_check_price').find('input[name="size"]').val();
	
	if($('.check_price #size_socks .socks_size').text() != "" && size_sock == ""){
		$('.check_price #size_socks .socks_size').css("border-bottom","1px solid red");
		prov = 1;
	}*/
	if(name == ""){
		$('#form_check_price').find('input[name="name"]').css("border","1px solid red");
		prov = 1;
	}
	if(kilkist == ""){
		$('#form_check_price').find('input[name="kilkist"]').css("border","1px solid red");
		prov = 1;
	}
	if(captcha == ""){
		$('#form_check_price').find('input[name="captcha"]').css("border","1px solid red");
		prov = 1;
	}
	
	if(phone == ""){
		$('#form_check_price').find('input[name="phone"]').css("border","1px solid red");
		prov = 1;
	}
	if (email == "" || atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
	{
		$('#form_check_price').find('input[name="email"]').css("border", "1px solid red" );
		prov = 1;
	}
	if(enquiry == ""){
		$('#form_check_price').find('textarea').css("border","1px solid red");
		prov = 1;
	}
	
	if(prov == 1){ return false; }
	else{
		jQuery.ajax({
			url: 'index.php?route=module/featured/check_price',
			type:     "POST", //Тип запроса
			dataType: "html", //Тип данных
			data: 	  jQuery("#"+'form_check_price').serialize(), 
			success: function(response) { //Если все нормально
				if(response == "error_captcha"){
					$('#form_check_price').find('input[name="captcha"]').css("border","1px solid red");
					$('.check_price #captcha').attr("src", "index.php?route=module/featured/captcha/?"+new Date().getTime());
					$('input[name="captcha"]').val('');
				}
				else{
					$('.check_price #captcha_text_success3').html("<div class='close2'>x</div>"+response);
					//console.log(response);
					$('.check_price #captcha_text_success3').addClass('success_call');
					$('.check_price #captcha_text_success3').fadeIn();
					document.getElementById('form_check_price').reset();
					$('.check_price .background_success').fadeIn();
					$('.check_price #captcha').attr("src", "index.php?route=module/featured/captcha/?"+new Date().getTime());
					$('.check_price #size_socks .socks_size').each(function(){
						$(this).removeClass('select');
					});
				}
				
			},
			error: function(response) {  
				//Если ошибка
				$('.check_price #captcha_text_success3').addClass('error_call');
				document.getElementById('captcha_text_success3').innerHTML = "Ошибка при отправке формы";
				$('.check_price #captcha_text_success3').fadeIn();
			}
		});
	}
}
</script>
<div class="check_price_background"></div>
<div class="check_price">
	<form action="" method="post" id="form_check_price" enctype="multipart/form-data">
		<h2 style="text-align: center;font-size: 18px; text-transform: uppercase; margin-bottom: 28px;font-family: arial;color: #000;"><?php echo $text_contact; ?><span class="close_form">x</span></h2>
		<div class="content" style="">
			<div class="img_name">
				<img src="">
				<div class="text_vsp">
					<span id="title_vsp"></span>
					<div id="size_socks"></div>
					
				</div>
				<div id="kolichestvo">
				<span style="float: right; font-size: 15px;">КОЛИЧЕСТВО:</span>
				<input type="text" name="kilkist" value="50">
				<span class="k_vo">шт.</span>
				</div>
				<input type="hidden" name="name_tov" >
				<input type="hidden" name="color_code" >
				<input type="hidden" name="color_name" >
				<input type="hidden" name="sku" >
			</div>
			<input type="hidden" name="opt" value="request_call">
			<div id="default_text">
				<span style="font-size: 15px; text-transform: uppercase; font-weight: bold;"><? echo $text_1; ?></span><br><span style="display: block; margin-bottom: 15px; font-size: 14px;"><? echo $text_2; ?></span>
			</div>
			<input class="stand_inp" style="" placeholder="*<?php echo $entry_name; ?>" type="text" name="name" />
			<br />
			<input class="stand_inp" placeholder="*<?php echo $entry_email; ?>" type="text" name="email" />
			<br />
			<input class="stand_inp" placeholder="*<?php echo $text_telephone; ?>" type="text" name="phone" />
			<br />
			<input class="stand_inp" placeholder="<?php echo $text_city; ?>" type="text" name="city" />
			<br />
		</div>
		<div style="margin:0 0 14px 0;    height: 38px;">
			<span class="span_captcha" ><img src="index.php?route=module/featured/captcha" alt="" id="captcha" /></span>
			<div class="reload_captcha"></div>
			<input class="stand_inp" style=" float: right; margin-top: -1px;width: 200px;" placeholder="*<?php echo $entry_captcha; ?>" type="text" name="captcha" value="" />
		</div>
		<div class="buttons">
			<!--div style="float: left; margin-top: 10px;">
				<input style="margin-right: 5px; margin-top: 1px; float: left;" type="checkbox" name="robot2" id="robokop2">
				<label style="display: block;float: left; font-size: 14px;  margin-right: 15px; color: #000; font-family: arial;" for="robokop2">Я не робот</label>
				<span id="robot2" style="color:red; font-size:14px; display:none;">Вы робот?</span>
			</div-->
			
			<input type="button" onClick="ValidFormCheckPrice();" value="<?php echo $text_contact; ?>" class="button_submit " />
		</div>
		<div id="captcha_text_success3"></div>
		<div class="background_success"></div>
	</form>
</div>
<div class="box">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content">
    <div class="box-product">
	<?// var_dump($products);?>
	<ul class="bxslider">
      <?php foreach ($products as $product) { ?>
      <div>
       <?php if ($product['thumb']) { ?>
          <div class="image"><a href="<? echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
        <?php } else { ?>
		  <div class="image"><a href="<? echo $product['href']; ?>"><img src="<?php echo $product['no_image']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
		<?php } ?>
		<!--div class="description-box"><?php echo $product['description']; ?></div-->
		<?php if ($product['rating']) { ?>
          <div class="rating"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></div>
        <?php } ?>
        <div class="name"><a href="<? echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
		
        <div class="box-bottom">
			<?if($product['attribute_groups']){ ?>
			<div style="display:none;" class="size_socks">
			<span >Размеры:</span>
			<select name="size" style="    color: #0b94e2;">			
			<?php
					foreach($product['attribute_groups'] as $attribute_group){
						//if($attribute_group['attribute_group_id'] == '7'){
							$koma = 0;
							foreach($attribute_group['attribute'] as $attribute){?>
								<option <? if($koma == 0) echo 'selected'; ?> value="<?php echo $attribute['name'].' / '.$attribute['text'] ?>">(<? echo $attribute['name']; ?>)/(<? echo $attribute['text']; ?>)</option>
							<? $koma++;
							}
						//}
					}
				?>
			</select>	
			</div>
			<?php } ?>
			<div style="    margin: 3px 0;"><a data-color_code="<?php echo $product['code_color'] ?>" data-color_name="<?php echo $product['name_color'] ?>" data-article="<?php if($product['sku']) echo $product['sku']; else echo 'Артикул отсутствует'; ?>"  class="tovar_socks" >Уточнить цену</a></div>
			<?php if ($product['quantity'] > 1) { ?>
				<div class="have_socks"><div>Есть в наличии</div></div>
			<? }else{ ?>
				<div class="not_have_socks"><div>Нет в наличии</div></div>
			<? } ?>
		  
		</div>
      </div>
			
		<?php } ?>
	  </ul>
    </div>

<script>
	$(document).ready(function(){
	  $('.bxslider').bxSlider({
		minSlides: 3,
		maxSlides: 4,
		moveSlides: 1,
		slideWidth: 224,
		slideMargin: 10,
		auto: true,
		pause: 6000,
		autoHover: true
	});
	});
</script>
  </div>
</div>
<?php
class ControllerModuleCategory2 extends Controller {
	protected function index($setting) {
		//var_dump($setting);
		$this->language->load('module/category2'); 

      	$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['setting'] = $setting;
		
		$this->document->addScript('catalog/view/javascript/jquery/tabs.js');
		$this->document->addScript('catalog/view/javascript/jquery/colorbox/jquery.colorbox-min.js');
		$this->document->addStyle('catalog/view/javascript/jquery/colorbox/colorbox.css');
		
		$this->data['button_quick_view'] = $this->language->get('button_quick_view');
		$this->data['button_cart'] = $this->language->get('button_cart');
		$this->data['button_wishlist'] = $this->language->get('button_wishlist');
		$this->data['button_compare'] = $this->language->get('button_compare');
		
		$this->data['quick_view_featured'] = $this->config->get('config_quick_view_featured');
		
		$this->data['top_bottom'] = $setting['position'] == 'content_top' || $setting['position'] == 'content_bottom';
		$this->data['side_left'] = $setting['position'] == 'column_left';
		$this->data['side_right'] = $setting['position'] == 'column_right';
		$this->data['side'] = $setting['position'] == 'column_left' || $setting['position'] == 'column_right';
		
		if ($this->config->get('config_limit_days_new_product')) {
			$limit_days_new_product = $this->config->get('config_limit_days_new_product');
		} else {
			$limit_days_new_product = 31;
		}
		
		if ($this->config->get('config_limit_viewed_popular_product')) {
			$limit_viewed_popular_product = $this->config->get('config_limit_viewed_popular_product');
		} else {
			$limit_viewed_popular_product = 50;
		}
		
		$this->load->model('catalog/product'); 
		
		$this->load->model('tool/image');

		$this->data['categories'] = array();

		$categories = explode(',', $this->config->get('category2'));		

		if (empty($setting['limit'])) {
			$setting['limit'] = 3;
		}
		//var_dump($categories);
		$key = count($categories);
		$key = $key - 1;
		while($key != -1){
			$categories2[] = $categories[$key];
			$key--;
		}
		$categories = array_slice($categories2, 0, (int)$setting['limit']);
		
		$timestamp 			= time();
		$date_time_array 	= getdate($timestamp);
		$hours 				= $date_time_array['hours'];
		$minutes 			= $date_time_array['minutes'];
		$seconds 			= $date_time_array['seconds'];
		$month 				= $date_time_array['mon'];
		$day 				= $date_time_array['mday'];
		$year 				= $date_time_array['year'];

		$timestamp = mktime($hours, $minutes, $seconds, $month,$day - $limit_days_new_product, $year);
		
		foreach ($categories as $category_id) {
			$product_info = $this->model_catalog_category->getCategory($category_id);
			//var_dump($product_info);
			if ($product_info) {
				if ($product_info['image']) {
					$image = $this->model_tool_image->resize($product_info['image'], 380, 190);
				} else {
					$image = false;
				}


				
				
				
					
				$this->data['categories'][] = array(
					'category_id'  => $product_info['category_id'],
					'thumb'   	  => $image,
					'no_image'    => $this->model_tool_image->resize('no_image.jpg', $setting['image_width'], $setting['image_height']),
					'name'    	  => $product_info['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_grid_description_limit')) ,
					'href'    	  => $this->url->link('product/category', 'path='. $product_info['parent_id'] . '_' . $product_info['category_id'])
				);
			}
		}
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/category2.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/category2.tpl';
		} else {
			$this->template = 'default/template/module/category2.tpl';
		}

		$this->render();
	}
}
?>
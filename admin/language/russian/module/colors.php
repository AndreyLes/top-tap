<?php
// Heading
$_['heading_title']       = 'Варианты цветов носков';

// Text
$_['text_module']         = 'Модули';
$_['text_success']        = 'Модуль цвет на главной успешно обновлен!';
$_['text_content_top']    = 'Содержание шапки';
$_['text_content_bottom'] = 'Содержание подвала';
$_['text_column_left']    = 'Левая колонка';
$_['text_column_right']   = 'Правая колонка';

// Entry
$_['entry_product']       = 'категории:<br /><span class="help">(Для автозаполнения введите знак %)</span>';
$_['entry_limit']         = 'Лимит:';
$_['entry_image']         = 'Изображение (Ширина x Высота):';
$_['entry_layout']        = 'Схема:';
$_['entry_position']      = 'Расположение:';
$_['entry_status']        = 'Статус:';
$_['entry_sort_order']    = 'Порядок сортировки:';
$_['color_name']   		  = 'Название цвета';
$_['color_code']   		  = 'Код цвета';

// Error 
$_['error_permission']    = 'Внимание: У Вас недостаточно прав для изменения модуля цвет на главной!';
$_['error_image']         = 'Введите рамеры изображения!';
?>
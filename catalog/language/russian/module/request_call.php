<?php
// Heading
$_['heading_title']  		= 'Служба поддержки';

// Text 
$_['text_location']  		= 'Реквизиты магазина';
$_['text_loc_store'] 		= 'Реквизиты (%s)';
$_['text_contact']   		= 'Заказать звонок';
$_['text_address']   		= 'Адрес:';
$_['text_email']     		= 'E-Mail:';
$_['button_continue'] 		= 'Отправить';

$_['text_fax']       		= 'Факс:';
$_['text_message']   		= '<p>Ваш запрос отправлен администрации магазина!</p>';
// Entry Fields
$_['text_telephone'] 		= 'Телефон';
$_['entry_name']     		= 'ФИО';
$_['entry_email']    		= 'E-Mail';
$_['entry_enquiry']  		= 'Комментарий';
$_['entry_captcha']  		= 'Введите код с картинки:';

// Email
$_['email_subject']  		= 'Обратный звонок с сайта ';

// Errors
$_['error_name']     		= 'ФИО должно быть от 3 до 32 символов!';
$_['error_email']    		= 'E-Mail введён неверно!';
$_['error_enquiry']  		= 'Длина текста должна быть от 10 до 3000 символов!';
$_['error_captcha']  		= 'Код с картинки введен неверно!'
?>

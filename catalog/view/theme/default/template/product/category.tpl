<?php echo $header; ?>
<div class="breadcrumb">
   <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
   <?php } ?>
</div>
<style>
@media (max-width: 1024px) { /* это будет показано при разрешении монитора до 930 пикселей */
	.request_call {
		position: absolute;
		top: 400px;
		left: 600px;
	}
	.check_price {
		position: absolute;
		top: 400px;
		left: 575px;
	}
	.zakaz_phone {
    position: absolute;
    left: 1150px;
    top: 455px;
}
}
</style>
<script>
$(window).load(function(){
	$('.tovar_socks').click(function(){
		var img = $(this).parent().parent().parent().find('.image img').attr('src');
		var name_tov = $(this).parent().parent().parent().find('.image img').attr('alt');
		var size_socks = $(this).parent().parent().find('.size_socks').html();
		$('.check_price .img_name').find('input[name="color_code"]').val($(this).attr('data-color_code'));
		$('.check_price .img_name').find('input[name="color_name"]').val($(this).attr('data-color_name'));
		$('.check_price .img_name').find('input[name="sku"]').val($(this).attr('data-article'));
		$('.check_price #size_socks').html(size_socks);
		$('.check_price .img_name').find('img').attr('src', img);
		$('.check_price .img_name').find('#title_vsp').text(name_tov);
		$('.check_price .img_name').find('input[name="name_tov"]').val(name_tov);
		$('.check_price #captcha').attr("src", "index.php?route=product/category/captcha/?"+new Date().getTime());
		$('.check_price_background, .check_price').fadeIn();
	});
	$('#container #wrapper').on('click', '.check_price_background, .close_form', function(){
		$('.check_price_background, .check_price').fadeOut();
		$('.check_price').fadeOut();
	});
	$('#form_check_price').find('input[name="phone"]').mask("+38(099)-999-99-99?");
	
	$('.check_price #size_socks').on('click', ".socks_size", function(){
		$('.check_price #size_socks .socks_size').each(function(){
			$(this).removeClass('select');
		});
		$(this).addClass('select');
		$('input[name="size"]').val($(this).text());
	});
	
	$('.category_socks').click(function(){
		var img = $(this).parent().parent().parent().find('.image img').attr('src');
		//alert(img);
		var name_tov = $(this).parent().parent().parent().find('.name a').text();
		$('.your_socks .img_name').find('img').attr('src', img);
		$('.your_socks .img_name').find('#title_vsp').text(name_tov);
		$('.your_socks .img_name').find('input[name="name_tov"]').val(name_tov);
		$('.your_socks #captcha').attr("src", "index.php?route=product/product/captcha/?"+new Date().getTime());
		$('.your_socks_background, .your_socks').fadeIn();
	});
	$('#container #wrapper').on('click', '.your_socks_background, .close_form', function(){
		$('.your_socks_background, .your_socks').fadeOut();
		$('.your_socks').fadeOut();
	});
	$('#form_your_socks').find('input[name="phone"]').mask("+38(099)-999-99-99?");
	
	$('#container #wrapper').on('click', '.your_socks .background_success, .your_socks .close2', function(){
		$('.background_success, .your_socks #captcha_text_success2').fadeOut();
		$('.your_socks #captcha_text_success2').fadeOut();
				$('.your_socks_background, .your_socks').fadeOut();
		$('.your_socks').fadeOut();
	});
	
	$('#container #wrapper').on('click', '.check_price .background_success, .check_price .close2', function(){
		$('.background_success, .check_price #captcha_text_success2').fadeOut();
		$('.check_price #captcha_text_success2').fadeOut();
		$('.check_price_background, .check_price').fadeOut();
		$('.check_price').fadeOut();
	});
	$('#container #wrapper').on('keyup keydown', 'input[name="kilkist"]', function(){
		if(this.value.toString().search(/[^0-9]/) != -1)
		this.value = this.value.toString().replace( /[^0-9]/g, '');
	});
	$('.check_price .reload_captcha').click(function(){
		$('.check_price #captcha').attr("src", "index.php?route=product/category/captcha/?"+new Date().getTime());
	});
	$('.your_socks .reload_captcha').click(function(){
		$('.your_socks #captcha').attr("src", "index.php?route=product/product/captcha/?"+new Date().getTime());
	});
});
function ValidFormCheckPrice(){
	$('.check_price div#size_socks').css("border","1px solid #fff");
	$('.check_price #size_socks .socks_size').css("border-bottom","1px solid #fff");
	$('#form_check_price input').css("border", "none")
	prov = 0;
	$('.check_price #captcha_text_success2').fadeOut();
	$('.check_price #captcha_text_success2').removeClass('error_call');
	//$('#robot2').fadeOut();
	$('#form_check_price input').css("border","1px solid #ccc");
	$('#form_check_price textarea').css("border","1px solid #ccc");
	var captcha = $('#form_check_price').find('input[name="captcha"]').val();
	var name = $('#form_check_price').find('input[name="name"]').val();
	var phone = $('#form_check_price').find('input[name="phone"]').val();
	var size_sock = $('#form_check_price').find('input[name="size"]').val();
	//var captcha_pr = $("#form_check_price").find('#robokop2').prop("checked");
	var kilkist = $('#form_check_price').find('input[name="kilkist"]').val();
	var email = $('#form_check_price').find('input[name="email"]').val();
	var enquiry = $('#form_check_price').find('textarea').val();
	var atpos = email.indexOf("@");
	var dotpos = email.lastIndexOf(".");
	
	/*if(captcha_pr == false){
		$('#robot2').fadeIn();
		prov = 1;
	}*/
	/*var size_sock = $('#form_check_price').find('input[name="size"]').val();
	if($('.check_price #size_socks .socks_size').text() != "" && size_sock == ""){
		$('.check_price #size_socks .socks_size').css("border-bottom","1px solid red");
		prov = 1;
	}*/
	if(kilkist == ""){
		$('#form_check_price').find('input[name="kilkist"]').css("border","1px solid red");
		prov = 1;
	}
	if(name == ""){
		$('#form_check_price').find('input[name="name"]').css("border","1px solid red");
		prov = 1;
	}
	if(captcha == ""){
		$('#form_check_price').find('input[name="captcha"]').css("border","1px solid red");
		prov = 1;
	}
	
	if(phone == ""){
		$('#form_check_price').find('input[name="phone"]').css("border","1px solid red");
		prov = 1;
	}
	if (email == "" || atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
	{
		$('#form_check_price').find('input[name="email"]').css("border", "1px solid red" );
		prov = 1;
	}
	if(enquiry == ""){
		$('#form_check_price').find('textarea').css("border","1px solid red");
		prov = 1;
	}
	
	if(prov == 1){ return false; }
	else{
		jQuery.ajax({
			url: 'index.php?route=product/category/check_price',
			type:     "POST", //Тип запроса
			dataType: "html", //Тип данных
			data: 	  jQuery("#"+'form_check_price').serialize(), 
			success: function(response) { //Если все нормально
				if(response == "error_captcha"){
					$('#form_check_price').find('input[name="captcha"]').css("border","1px solid red");
					$('.check_price #captcha').attr("src", "index.php?route=product/category/captcha/?"+new Date().getTime());
					$('input[name="captcha"]').val('');
				}
				else{
					$('.check_price #captcha_text_success2').html("<div class='close2'>x</div>"+response);
					$('.check_price #captcha_text_success2').addClass('success_call');
					$('.check_price #captcha_text_success2').fadeIn();
					document.getElementById('form_check_price').reset();
					$('.check_price .background_success').fadeIn();
					$('.check_price #captcha').attr("src", "index.php?route=product/category/captcha/?"+new Date().getTime());
					$('.check_price #size_socks .socks_size').each(function(){
						$(this).removeClass('select');
					});
				}
				
			},
			error: function(response) {  
				//Если ошибка
				$('#captcha_text_success2').addClass('error_call');
				document.getElementById('captcha_text_success2').innerHTML = "Ошибка при отправке формы";
				$('#captcha_text_success2').fadeIn();
			}
		});
	}
}
function ValidFormCheckPrice2(){
	$('.your_socks div#size_socks').css("border","1px solid #fff");
	$('#form_your_socks input').css("border", "none")
	prov = 0;
	$('.your_socks #captcha_text_success2').fadeOut();
	$('.your_socks #captcha_text_success2').removeClass('error_call');
	//$('#robot2').fadeOut();
	$('#form_your_socks input').css("border","1px solid #ccc");
	$('#form_your_socks textarea').css("border","1px solid #ccc");
	var captcha = $('#form_your_socks').find('input[name="captcha"]').val();
	var name = $('#form_your_socks').find('input[name="name"]').val();
	var phone = $('#form_your_socks').find('input[name="phone"]').val();
	//var captcha_pr = $("#form_your_socks").find('#robokop2').prop("checked");
	var city = $('#form_your_socks').find('input[name="city"]').val();
	var email = $('#form_your_socks').find('input[name="email"]').val();
	var atpos = email.indexOf("@");
	var dotpos = email.lastIndexOf(".");
	
	if(name == ""){
		$('#form_your_socks').find('input[name="name"]').css("border","1px solid red");
		prov = 1;
	}
	if(captcha == ""){
		$('#form_your_socks').find('input[name="captcha"]').css("border","1px solid red");
		prov = 1;
	}
	if(phone == ""){
		$('#form_your_socks').find('input[name="phone"]').css("border","1px solid red");
		prov = 1;
	}
	if (email == "" || atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
	{
		$('#form_your_socks').find('input[name="email"]').css("border", "1px solid red" );
		prov = 1;
	}
	
	if(prov == 1){ return false; }
	else{
		jQuery.ajax({
			url: 'index.php?route=product/category/your_socks',
			type:     "POST", //Тип запроса
			dataType: "html", //Тип данных
			data: 	  jQuery("#"+'form_your_socks').serialize(), 
			success: function(response) { //Если все нормально
				if(response == "error_captcha"){
					$('#form_your_socks').find('input[name="captcha"]').css("border","1px solid red");
					$('.your_socks #captcha').attr("src", "index.php?route=product/product/captcha/?"+new Date().getTime());
					$('input[name="captcha"]').val('');
				}
				else{
					$('.your_socks #captcha_text_success2').html("<div class='close2'>x</div>"+response);
					$('.your_socks #captcha_text_success2').addClass('success_call');
					$('.your_socks #captcha_text_success2').fadeIn();
					document.getElementById('form_your_socks').reset();
					$('.your_socks .background_success').fadeIn();
					$('.your_socks #captcha').attr("src", "index.php?route=product/product/captcha/?"+new Date().getTime());
				}
			},
			error: function(response) {  
				//Если ошибка
				alert("Ошибка");
				$('#captcha_text_success2').addClass('error_call');
				document.getElementById('captcha_text_success2').innerHTML = "Ошибка при отправке формы";
				$('#captcha_text_success2').fadeIn();
			}
		});
	}
}
</script>
<div class="check_price_background"></div>
<div class="your_socks_background"></div>
<style>
.your_socks .text_vsp {
	float: right;
    width: 240px;
}
.your_socks .img_name img {
    width: 190px;
    max-height: 120px;
}
</style>
<div class="your_socks">
	<form action="" method="post" id="form_your_socks" enctype="multipart/form-data">
		<h2 style="text-align: center;font-size: 18px; text-transform: uppercase; margin-bottom: 28px;font-family: arial;color: #000;"><?php echo $text_contact; ?><span class="close_form">x</span></h2>
		<div class="content" style="">
			<div class="img_name">
				<img src="">
				<div class="text_vsp">
					<span id="title_vsp"></span>
				</div>
				<input type="hidden" name="name_tov" >
			</div>
			<input type="hidden" name="color_tov" >
			<input type="hidden" name="opt" value="request_call">
			<div id="default_text">
				<span style="font-size: 15px; text-transform: uppercase; font-weight: bold;"><? echo $text_1; ?></span><br><span style="display: block; margin-bottom: 15px; font-size: 14px;"><? echo $text_2; ?></span>
			</div>
			<input class="stand_inp" style="" placeholder="*<?php echo $entry_name; ?>" type="text" name="name" />
			<br />
			<input class="stand_inp" placeholder="*<?php echo $entry_email; ?>" type="text" name="email" />
			<br />
			<input class="stand_inp" placeholder="*<?php echo $text_telephone; ?>" type="text" name="phone" />
			<br />
			<input class="stand_inp" placeholder="<?php echo $text_city; ?>" type="text" name="city" />
			<br />
		</div>
		<div style="margin:0 0 14px 0;    height: 38px;">
			<span class="span_captcha" ><img src="index.php?route=product/product/captcha" alt="" id="captcha" /></span>
			<div class="reload_captcha"></div>
			<input class="stand_inp" style=" float: right; margin-top: -1px;width: 200px;" placeholder="*<?php echo $entry_captcha; ?>" type="text" name="captcha" value="" />
		</div>
		<div class="buttons">
			
			<input type="button" onClick="ValidFormCheckPrice2();" value="Уточнить цену" class="button_submit " />
		</div>
		<div id="captcha_text_success2"></div>
		<div class="background_success"></div>
	</form>
</div>
<div class="check_price">
	<form action="" method="post" id="form_check_price" enctype="multipart/form-data">
		<h2 style="text-align: center;font-size: 18px; text-transform: uppercase; margin-bottom: 28px;font-family: arial;color: #000;"><?php echo $text_contact; ?><span class="close_form">x</span></h2>
		<div class="content" style="">
			<div class="img_name">
				<img src="">
				<div class="text_vsp">
					<span id="title_vsp"></span>
					<div id="size_socks"></div>
				</div>
				<div id="kolichestvo">
				<span style="float: right; font-size: 15px;">КОЛИЧЕСТВО:</span>
				<input type="text" name="kilkist" value="50">
				<span class="k_vo">шт.</span>
				</div>
				<input type="hidden" name="name_tov" >
				<input type="hidden" name="color_code" >
				<input type="hidden" name="color_name" >
				<input type="hidden" name="sku" >
			</div>
			<input type="hidden" name="opt" value="request_call">
			<div id="default_text">
				<span style="font-size: 15px; text-transform: uppercase; font-weight: bold;"><? echo $text_1; ?></span><br><span style="display: block; margin-bottom: 15px; font-size: 14px;"><? echo $text_2; ?></span>
			</div>
			<input class="stand_inp" style="" placeholder="*<?php echo $entry_name; ?>" type="text" name="name" />
			<br />
			<input class="stand_inp" placeholder="*<?php echo $entry_email; ?>" type="text" name="email" />
			<br />
			<input class="stand_inp" placeholder="*<?php echo $text_telephone; ?>" type="text" name="phone" />
			<br />
			<input class="stand_inp" placeholder="<?php echo $text_city; ?>" type="text" name="city" />
			<br />
		</div>
		<div style="margin:0 0 14px 0;    height: 38px;">
			<span class="span_captcha" ><img src="index.php?route=product/category/captcha" alt="" id="captcha" /></span>
			<div class="reload_captcha"></div>
			<input class="stand_inp" style=" float: right; margin-top: -1px;width: 200px;" placeholder="*<?php echo $entry_captcha; ?>" type="text" name="captcha" value="" />
		</div>
		<div class="buttons">
			<!--div style="float: left; margin-top: 10px;">
				<input style="margin-right: 5px; margin-top: 1px; float: left;" type="checkbox" name="robot2" id="robokop2">
				<label style="display: block;float: left; font-size: 14px;  margin-right: 15px; color: #000; font-family: arial;" for="robokop2">Я не робот</label>
				<span id="robot2" style="color:red; font-size:14px; display:none;">Вы робот?</span>
			</div-->
			
			<input type="button" onClick="ValidFormCheckPrice();" value="<?php echo $text_contact; ?>" class="button_submit " />
		</div>
		<div id="captcha_text_success2"></div>
		<div class="background_success"></div>
	</form>
</div>

<div id="content"><?php echo $content_top; ?>
  <!--h1><?php echo $heading_title; ?></h1-->
  <div class="main-content">
  <?php if ($thumb || $description) { ?>
  <!--div class="category-info">
    <?php if ($thumb) { ?>
    <div class="image"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" /></div>
    <?php } ?>
    <?php if ($description) { ?>
    <?php echo $description; ?>
    <?php } ?>
  </div-->
  <?php } ?>
  
  <?php /*=================== если нужно выводить подкатегории ==================*/ ?>
  <?php if ($categories) { ?>
  <?php if ($view_subcategories == 'images') { ?>
  <style>
	.your_socks .content input {
		width: 92.5%;
		margin-bottom: 15px;
		border: 1px solid #fff;
	}
  </style>
  <script>
	
  </script>
    <div class="box-category">
	  <!--div class="box-heading-subcategory"><?php echo $text_refine; ?></div-->
      <div class="box-subcategory">
	    <?php foreach ($categories as $category) { ?>
		  <div style="width: <?php echo $width; ?>px;">
		    <?php if ($category['thumb']) { ?>
			  <div class="image"><a style="display: block;" href="<?php echo $category['href']; ?>"><img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name1']." ".$category['name2']; ?>" /></a></div>
		    <?php } else { ?>
			  <div class="image"><a style="display: block;" href="<?php echo $category['href']; ?>"><img src="<?php echo $category['no_image']; ?>" alt="<?php echo $category['name1']." ".$category['name2']; ?>" /></a></div>
		    <?php } ?>
			<!--div class="description-box"><?php echo $category['description']; ?></div-->
			<div class="name"><a href="<?php echo $category['href']; ?>"><?php echo "<span>".$category['name1']."</span><p style='margin-bottom: 0;'>".$category['name2']."</p>"; ?></a></div>
			<div class="box-bottom">
				<div style="    margin: 3px 0;"><a  class="category_socks" >Уточнить цену</a></div>
				<?php if ($category['product_total'] >= 1) { ?>
					<div class="have_socks"><div>Есть в наличии</div></div>
				<? }else{ ?>
					<div class="not_have_socks"><div>Нет в наличии</div></div>
				<? } ?>
			  
			</div>
		  </div>
		<?php } ?>
	  </div>
    </div>
  <?php } ?>
  <?php if ($view_subcategories == 'default') { ?>
    <div class="category-list">
	  <div class="box-category">
	    <div class="box-heading-subcategory"><?php echo $text_refine; ?></div>
		<?php if (count($categories) <= 5) { ?>
		  <ul>
           <?php foreach ($categories as $category) { ?>
              <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
            <?php } ?>
		  </ul>
	    <?php } else { ?>
		  <?php for ($i = 0; $i < count($categories);) { ?>
		    <ul>
			  <?php $j = $i + ceil(count($categories) / 4); ?>
			  <?php for (; $i < $j; $i++) { ?>
			    <?php if (isset($categories[$i])) { ?>
				  <li><a href="<?php echo $categories[$i]['href']; ?>"><?php echo $categories[$i]['name']; ?></a></li>
			    <?php } ?>
			  <?php } ?>
		    </ul>
		  <?php } ?>
	    <?php } ?>
	  </div>
	</div>
  <?php } ?>
  <?php } ?>
   <?php /*=================== если нужно выводить продукты ==================*/ ?>
  <?php if ($products && !$categories) { ?>
  <div class="product-filter">
    <div class="sort"><b style="margin: 0 45px;"><?php echo $text_sort; ?></b>
      <select onchange="location = this.value;" style="    border-radius: 0;  <? if(isset($_GET['color'])){?>background: #ffcc00;<?}else{?>  background: none;<?}?> box-shadow: none;">
		<option value="<?php echo $special_sort['href']; ?><? if(isset($_GET['sort']) && isset($_GET['order'])){ echo "&sort=".$_GET['sort']."&order=".$_GET['order']; }?>" selected="selected">Все цвета</option>
        <?php foreach ($sorts as $sorts) { ?>
        <?php if ($sorts['value'] == $color) { ?>
        <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
        <?php } ?>
        <?php } ?>
      </select>
    </div>
	<div class="quantity_href">
		<a <? if(isset($_GET['availability']) && $_GET['availability'] == '1'){?>style="background: #ffcc00; background-image: url(catalog/view/theme/default/image/fon/galka.png) ; background-repeat:no-repeat; background-position: 10px 9px;"<?;}else{?>style=" background-image: url(catalog/view/theme/default/image/fon/x.png) ; background-repeat:no-repeat; background-position: 10px 9px;"<?}?> class="on" href="<? echo $quantity; ?>&availability=1">В наличии</a>
	</div>
	<div class="quantity_href">
		<a <? if(isset($_GET['availability']) && $_GET['availability'] == '2'){?>style="background: #ffcc00; background-image: url(catalog/view/theme/default/image/fon/galka.png) ; background-repeat:no-repeat; background-position: 7px 9px;"<?;}else{?>style=" background-image: url(catalog/view/theme/default/image/fon/x.png) ; background-repeat:no-repeat; background-position: 7px 9px;"<?}?> class="off" href="<? echo $quantity; ?>&availability=2">Нет в наличии</a>
	</div>
	<div class="quantity_href">
		<a class="reset_sorting" href="<? echo $reset_sorting; ?>">Сбросить все</a>
	</div>
  </div>
  <div class="product-grid">
    <?php foreach ($products as $product) { ?>
    <div>
      <?php if ($product['thumb']) { ?>
      <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
      <?php } else { ?>
	  <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['no_image']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
	  <?php } ?>
      <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
      
		
	 <div class="box-bottom">
		<?if($product['attribute_groups']){ ?>
			<div style="display:none;" class="size_socks">
			<span>Размеры:</span>	
			<select name="size" style="    color: #0b94e2;">			
			<?php
					foreach($product['attribute_groups'] as $attribute_group){
						//if($attribute_group['attribute_group_id'] == '7'){
							$koma = 0;
							foreach($attribute_group['attribute'] as $attribute){?>
								<option <? if($koma == 0) echo 'selected'; ?> value="<?php echo $attribute['name'].' / '.$attribute['text'] ?>">(<? echo $attribute['name']; ?>)/(<? echo $attribute['text']; ?>)</option>
							<? $koma++;
							}
						//}
					}
				?>
			</select>	
			</div>
			<?php } ?>
			<div style="    margin: 3px 0;"><a data-color_code="<?php echo $product['code_color'] ?>" data-color_name="<?php echo $product['name_color'] ?>" data-article="<?php if($product['sku']) echo $product['sku']; else echo 'Артикул отсутствует'; ?>" class="tovar_socks" >Уточнить цену</a></div>
			<?php if ($product['quantity'] != 0) { ?>
				<div class="have_socks"><div>Есть в наличии</div></div>
			<? }else{ ?>
				<div class="not_have_socks"><div>Нет в наличии</div></div>
			<? } ?>
		  
		</div>
    </div>
    <?php } ?>
  </div>
  <div class="pagination"><?php echo $pagination; ?></div>
  <?php echo $content_bottom; ?>
  <?php } ?>
  <?php if (!$categories && !$products) { ?>
  <div class="content">
  <div class="product-filter">
    <div class="sort"><b style="margin: 0 45px;"><?php echo $text_sort; ?></b>
      <select onchange="location = this.value;" style="    border-radius: 0;  <? if(isset($_GET['color'])){?>background: #ffcc00;<?}else{?>  background: none;<?}?> box-shadow: none;">
		<option value="<?php echo $special_sort['href']; ?><? if(isset($_GET['sort']) && isset($_GET['order'])){ echo "&sort=".$_GET['sort']."&order=".$_GET['order']; }?>" selected="selected">Все цвета</option>
        <?php foreach ($sorts as $sorts) { ?>
        <?php if ($sorts['value'] == $color) { ?>
        <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
        <?php } ?>
        <?php } ?>
      </select>
    </div>
	<div class="quantity_href">
		<a <? if(isset($_GET['availability']) && $_GET['availability'] == '1'){?>style="background: #ffcc00; background-image: url(catalog/view/theme/default/image/fon/galka.png) ; background-repeat:no-repeat; background-position: 10px 11px;"<?;}else{?>style=" background-image: url(catalog/view/theme/default/image/fon/x.png) ; background-repeat:no-repeat; background-position: 10px 11px;"<?}?> class="on" href="<? echo $quantity; ?>&availability=1">В наличии</a>
	</div>
	<div class="quantity_href">
		<a <? if(isset($_GET['availability']) && $_GET['availability'] == '2'){?>style="background: #ffcc00; background-image: url(catalog/view/theme/default/image/fon/galka.png) ; background-repeat:no-repeat; background-position: 7px 11px;"<?;}else{?>style=" background-image: url(catalog/view/theme/default/image/fon/x.png) ; background-repeat:no-repeat; background-position: 7px 11px;"<?}?> class="off" href="<? echo $quantity; ?>&availability=2">Нет в наличии</a>
	</div>
	<div class="quantity_href">
		<a class="reset_sorting" href="<? echo $reset_sorting; ?>">Сбросить все</a>
	</div>
  </div>
  <span style="font-size: 17px;"><?php echo $text_empty; ?></span>
  </div>
  <!--div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
  </div-->
  <?php } ?>
  </div>
  
  </div>
<script type="text/javascript"><!--
function display(view) {
	/*if (view == 'list') {
		$('.product-grid').attr('class', 'product-list');
		
		$('.product-list > div').each(function(index, element) {	
			html  = '';
			
			var image = $(element).find('.image').html();
			
			if (image != null) { 
				html += '<div class="image">' + image + '</div>';
			}
			
			html += '<div class="cart-box">' + $(element).find('.cart-box').html() + '</div>';
			
			html += '  <div class="name">' + $(element).find('.name').html() + '</div>';
			
			var price = $(element).find('.price').html();
			
			if (price != null) {
				html += '<div class="price">' + price  + '</div>';
			}
			
			html += '  <div class="description-list">' + $(element).find('.description-list').html() + '</div>';
			html += '  <div class="description-grid">' + $(element).find('.description-grid').html() + '</div>';
						
			$(element).html(html);
		});	
			
		$('.poshytips').poshytip({
			className: 'tip-twitter',
			showTimeout: 1,
			alignTo: 'target',
			alignX: 'center',
			offsetY: 5,
			allowTipHover: false
		});
		
		$('.colorbox').colorbox({
			overlayClose: true,
			opacity: 0.5,
			width:"1050px",
			height:"750px",
			fixed:true,
			rel: "colorbox"
		});
		
		$('.display').html('<?php echo $text_display; ?>&nbsp;<img align="absmiddle" src="catalog/view/theme/default/image/icon/list-icon-active.png">&nbsp;<a onclick="display(\'grid\');"><img align="absmiddle" src="catalog/view/theme/default/image/icon/grid-icon.png" title="<?php echo $text_grid; ?>"></a>');
		
		$.totalStorage('display', 'list'); 
	} else {*/
		$('.product-list').attr('class', 'product-grid');
		
		$('.product-grid > div').each(function(index, element) {
			html = '';
			
			var image = $(element).find('.image').html();
			
			if (image != null) {
				html += '<div class="image">' + image + '</div>';
			}
			
			/*html += '  <div class="description-list">' + $(element).find('.description-list').html() + '</div>';
			html += '<div class="description-grid">' + $(element).find('.description-grid').html() + '</div>';*/
			
			var rating = $(element).find('.rating').html();
			
			if (rating != null) {
				html += '<div class="rating">' + rating + '</div>';
			}
			
			html += '<div class="name">' + $(element).find('.name').html() + '</div>';
			
			var price = $(element).find('.price').html();
			
			if (price != null) {
				html += '<div class="price">' + price  + '</div>';
			}
			
			html += '<div class="box-bottom">' + $(element).find('.box-bottom').html() + '</div>';

			//html += '<div class="cart-box">' + $(element).find('.cart-box').html() + '</div>';
			
			$(element).html(html);
		});	
		
		$('.poshytips').poshytip({
			className: 'tip-twitter',
			showTimeout: 1,
			alignTo: 'target',
			alignX: 'center',
			offsetY: 5,
			allowTipHover: false
		});
		
		$('.colorbox').colorbox({
			overlayClose: true,
			opacity: 0.5,
			width:"1050px",
			height:"750px",
			fixed:true,
			rel: "colorbox"
		});
					
		/*$('.display').html('<?php echo $text_display; ?>&nbsp;<a onclick="display(\'list\');"><img align="absmiddle" src="catalog/view/theme/default/image/icon/list-icon.png" title="<?php echo $text_list; ?>"></a>&nbsp;<img align="absmiddle" src="catalog/view/theme/default/image/icon/grid-icon-active.png">');

		$.totalStorage('display', 'grid');*/
	//}
}

view = $.totalStorage('display');

if (view) {
	display(view);
} else {
	display('list');
}
//--></script> 
<?php echo $footer; ?>